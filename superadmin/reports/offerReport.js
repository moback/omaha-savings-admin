/**
 * Created by user on 4/4/16.
 */
angular.module('superadmin')
  .controller('offerReportCtrl', function (userService,$location,$cookies,$uibModal,$state,apiomahaService,$q,$route,NgTableParams,$scope) {

    var vm = this;
    vm.usersChunk = [];
    vm.usersEmail = [];
    vm.userdata = [];
    vm.offerRed = [];
    vm.totalusers = 0;
    vm.finalusers = [];
    vm.ziplist;
    vm.usersEmaildata = usersEmaildatacsv;
    vm.offerreddata = offerreddatacsv
    vm.usersEmailclick = useremailbt;



    vm.reportType = ['Export Offer-New','Export Users Emails','Export Offer Redeemed by Zip','Generate Redeemed Offers Report'];
    vm.rtype = vm.reportType[2];
    vm.startDateTime = new Date();
    vm.actstartdatePickerIsOpen = false;
    vm.endDateTime = new Date();
    vm.actenddatePickerIsOpen = false;
    //vm.runreport = generatereport;
    vm.totalrecords = 0;
    vm.isHtmlReport =  false;
    vm.ngcsvfunc = "csvdata";
    vm.exportUseresCsv = exportUsers;
    vm.exportOffersCsv = exportRedOffers;
    vm.exportUserAndOffer = exportUserAndOffer;
    vm.runreport = genRedOffers
    vm.orderlist =  [];
    // vm.editRecord = editModal;
    //vm.gettexttype = gettexttypeModel;

    init();

    function init(){
     // getusersEmail();
     // getofferlist();

    }

/*
    function generatereport(){

      if (vm.startDateTime <= vm.endDateTime){

        switch (vm.rtype) {
          case 'Export Users Emails':
            return exportUsers();
            break;
          case 'Export offer Red. by Zip':
            exportRedOffers();
            break;
          case 'Generate Redeemed Offers Report':
            genRedOffers();
            break;
        }
      }
      else{

        alert('Invalid From Date - To Date range value');

      }

    }
*/


    vm.actstartdatePickerOpen = function ($event) {

      if ($event) {
        $event.preventDefault();
        $event.stopPropagation();
      }
      this.actstartdatePickerIsOpen = true;
    };

    vm.opens = [];
    $scope.$watch(function () {
      return vm.actenddatePickerIsOpen;
    },function(value){
      vm.opens.push("actenddatePickerIsOpen: " + value + " at: " + new Date());
    });

    vm.actenddatePickerOpen = function ($event) {

      if ($event) {
        $event.preventDefault();
        $event.stopPropagation();
      }
      this.actenddatePickerIsOpen = true;
    };

    function exportUserAndOffer(){
      console.log("asdasd");

      var users = exportUsers();
      console.log(users);

    }

    function exportUsers(){

      vm.dataLoading = true;

      vm.startDateTime.setHours( 0,0,0,0);
      vm.endDateTime.setHours( 23,59,0,0 );

      vm.startDateTime.setHours( 0,0,0,0);
      vm.endDateTime.setHours( 23,59,0,0 );

      promises = [];
      vm.usrobjarray = [];


      return userService.getUserInDateRangeCount(vm.startDateTime.toISOString(),vm.endDateTime.toISOString()).then(function(data){

        if(data.data.count > 0 ){
          vm.totalrecords = data.data.count;
          for(var i=0; i < Math.ceil(vm.totalrecords/500); i++) {
              promises.push(exportUsersGetData(vm.startDateTime.toISOString(),vm.endDateTime.toISOString(),i*500));
          }

          return $q.all(promises).then(function(){

            var exportUsersdata = _.sortBy(vm.usrobjarray, function(o) { return o.fName + o.lName; })




            exportUsersdata.unshift(['First Name', 'Last Name','Zip', 'Email','Allowed to receive Emails','Disabled','Local / Regional']);

            vm.dataLoading = false;
            
            return exportUsersdata;
          });
        }

      });

    };


    function exportUsersGetData(stdate,enddate,skiprec){


      var d = $q.defer();
      return userService.getUserInDateRange(stdate,enddate,skiprec).then(function(data) {

        if(data.data && data.data.results.length >= 0){

          var csvdata = _.map(data.data.results, function(currentObject) {


            if (! _.has(currentObject, "disabled")) {
              currentObject['disabled'] = "NO";
            }

            if (currentObject.disabled == true){
               currentObject['disabled'] = "YES";
            }

            if (currentObject.disabled == false){
              currentObject['disabled'] = "NO";
            }

            if (currentObject.allowedFutureEmails == true){
                currentObject['allowedFutureEmails'] = "YES";
            }
            else
            {
              currentObject['allowedFutureEmails'] = "NO";
            }

            var finalusrobjlist =  _.pick(currentObject, 'fName','lName','zip','email','allowedFutureEmails','disabled','zipCodeType');
            vm.usrobjarray.push(_.pick(currentObject, 'fName','lName','zip','email','allowedFutureEmails','disabled','zipCodeType'));


            return finalusrobjlist;
          });


          d.resolve();
          return d.promise;
        }

      });

    };


    function exportRedOffers(){

      vm.dataLoading = true;

      vm.startDateTime.setHours( 0,0,0,0);
      vm.endDateTime.setHours( 23,59,0,0 );

      vm.startDateTime.setHours( 0,0,0,0);
      vm.endDateTime.setHours( 23,59,0,0 );

      promises = [];
      vm.offerbjarray = [];


      return apiomahaService.getOffersInDateRangeCount(vm.startDateTime.toISOString(),vm.endDateTime.toISOString()).then(function(data){



        if(data.data.count > 0 ){
          vm.totalrecords = data.data.count;


          for(var i=0; i < Math.ceil(vm.totalrecords/500); i++) {
            promises.push(exportOfferGetData(vm.startDateTime.toISOString(),vm.endDateTime.toISOString(),i*500));
          }

          return $q.all(promises).then(function(){

            var exportOffersdata = _.sortBy(vm.offerbjarray, function(o) { return o.name; })


            exportOffersdata.unshift(['Partner Name','Offer Title','First Name','Last Name','Zip Code','Email Address','Redemption Quantity','Redemption Date','Redemption Time']);

            vm.dataLoading = false;
            return exportOffersdata;
          });
        }

      });


    };




    function exportOfferGetData(stdate,enddate,skiprec){

      console.log('inside offer');
      console.log(skiprec);

      var d = $q.defer();
      return apiomahaService.getOffersInDateRange(stdate,enddate,skiprec,500).then(function(data) {

        if(data.data && data.data.results.length >= 0){




          var csvdata = _.map(data.data.results, function(currentObject) {


            var createddateobj = new Object();
            var vdate = new Date(currentObject.createdAt);
            createddateobj.createddate = vdate.toLocaleDateString();
            createddateobj.createdtime = vdate.toTimeString();



            var filteredpartnerobj = new Object();
            if (currentObject.partner != null){
              var partnerobj = _.flatten(_.pick(currentObject, 'partner'));
              filteredpartnerobj.name = partnerobj[0].name;
            }


            var filteredofferobj = new Object();
            if (currentObject.offer != null) {
              var offerobj = _.flatten(_.pick(currentObject, 'offer'));
              filteredofferobj.title = offerobj[0].title;
              //filteredpartnerobj = _.pick(offerobj[0],'title');
            }


            var filteredUser = new Object();
            if (currentObject.user != null) {
              var userobj = _.flatten(_.pick(currentObject, 'user'));
              filteredUser = _.pick(userobj[0],'fName','lName','zip','email');
            }



            var exportOffersdata =  _.pick(currentObject,'qty');
            vm.offerbjarray.push(_.extend(filteredpartnerobj,filteredofferobj, filteredUser,exportOffersdata,createddateobj));


            return exportOffersdata;
          });





          d.resolve();
          return d.promise;
        }

      });

    };



    function genRedOffers(){


      console.log('inside func');



      vm.dataLoading = true;

      vm.startDateTime.setHours( 0,0,0,0);
      vm.endDateTime.setHours( 23,59,0,0 );

      vm.startDateTime.setHours( 0,0,0,0);
      vm.endDateTime.setHours( 23,59,0,0 );

      promises = [];
      vm.offerbjarray = [];


      apiomahaService.getOffersInDateRangeCount(vm.startDateTime.toISOString(),vm.endDateTime.toISOString()).then(function(data){



        if(data.data.count > 0 ){
          vm.totalrecords = data.data.count;


          vm.offerReportTable = new NgTableParams({
            page: 1,            // show first page
            count: 100,          // count per page
            sorting: {
              createdAt: 'asc'     // initial sorting
            }
          }, {
            total: vm.totalrecords, // length of data
            getData: function($defer, params) {

              var page = params.page();
              var size = params.count();
              //var filter = params.filter();

              console.log('parameters ');
              console.log(params);
              console.log('page - >'+params.page());
              console.log('count - >'+params.count());
              console.log('sorting - >'+params.sorting);
              console.log('sorting - >'+params.orderBy());

              console.log(params.settings());

              console.log('end parameters ');




              apiomahaService.getOffersInDateRangeSort(vm.startDateTime.toISOString(),vm.endDateTime.toISOString(),(page - 1) * params.count(),params.count(),params.orderBy()).then(function(newdata) {

                console.log('Total records fatched in report->' + newdata.data.results.length);
                vm.dataLoading = false;
                if (newdata.data && newdata.data.results.length >= 0) {
                  vm.orderlist = newdata.data.results;

                  /*
                  var orderedData = params.sorting() ?
                    $filter('orderBy')(vm.orderlist, params.orderBy()) :
                    vm.orderlist;
                  */
                  var orderedData = vm.orderlist;

                  $defer.resolve(orderedData);

                 // $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

                }
              });


            }


          });

        }

      });


    }


    function useremailbt() {

      var combinedUsers = _.flatten(vm.userdata,true);
      return combinedUsers;

    }


    function getusersEmail() {


      apiomahaService.getmobackdata('appSettings').then(function(data){


        //vm.objId = data.data.results[0].objectId;
        vm.ziplist = data.data.results[0].localZipcodes;



        if(data.data && data.data.results.length >= 0){

          //console.log('User data');
          //console.log(data.data.results.length);


          apiomahaService.recordCount().then(function(data){

            if(data.data){

              vm.totalusers = data.data[5].count;
              for (i = 1; i < vm.totalusers; i = i+500) {

                userService.getUsersWithCondition('allowedFutureEmails','true',i).then(function(data){

                  if(data.data && data.data.results.length >= 0){


                    vm.usersEmail.push(data.data.results);

                    vm.usersChunk = _.map(data.data.results, function(currentObject) {

                      var localregional = "Regional";
                      if (vm.ziplist.toString().includes(currentObject.zip))
                      {
                        localregional = "local";
                      }

                      return [currentObject.fName,currentObject.lName,currentObject.zip,currentObject.email,currentObject.allowedFutureEmails,localregional];

                    });
                    vm.userdata.push(vm.usersChunk);


                  }


                });

              } // end for

            }
          });

        }
      });






      /*
      userService.getUsersWithCondition('allowedFutureEmails','true').then(function(data){

        if(data.data && data.data.results.length >= 0){

          console.log('user data');
          console.log(data.data);
          vm.usersEmail = _.map(data.data.results, function(currentObject) {

            return [currentObject.fName,currentObject.lName,currentObject.zip,currentObject.email,currentObject.allowedFutureEmails];

          });

        }
      });
      */





    }

    // get all textMain
    function getofferlist(){

      apiomahaService.getRedeemedOffers().then(function(data){

        if(data.data && data.data.results.length >= 0){

          //console.log('offer data');
          //console.log(data.data.results.length);

          vm.offerRed = _.map(data.data.results, function(currentObject) {

            var vdate = new Date(currentObject.createdAt);
            var vformatedDate = vdate.getMonth()+1 + "/" + vdate.getDate() + "/" + vdate.getYear();
            //var offerObj = _.pluck(currentObject.offer,'title');

            var vofferTitle;
            _.each( currentObject.offer, function( val, key ) {
              if ( key == 'title' ) {
                vofferTitle = val;
              }
            });
           // console.log(currentObject.objectId);
           // console.log(currentObject.partner);


            var partnerName="";
            if (currentObject.partner != null)
            {
              partnerName = currentObject.partner.name;
            }

            var userZip = "";
            var userEmail = "";
            if (currentObject.user != null)
            {
              var userZip = currentObject.user.zip;
              var userEmail = currentObject.user.email;
            }



            //console.log(partnerName);
            return [partnerName,vofferTitle,userZip,userEmail,vformatedDate,currentObject.qty];

          });


          vm.offerRed = _.sortBy(vm.offerRed, '0');
         // var offerReddata = _.pick(data.data.results,'qty','partner');
          vm.offerReportTable = new NgTableParams({
            page: 1,
            count: 10
          }, { data: data.data.results });

        }
      });



    }






    // csv export step 1

    function usersEmaildatacsv()
    {
      vm.dataLoading = true;
      return gettotaluserrecords();


    }


    // csv export step 2 of 5
    // csv export step 3 of 5
    function gettotaluserrecords()
    {

      return apiomahaService.gettotalrecords('__appUsers').then(function(data) {

        if(data.data.count >= 0){
          vm.totalallrecord = data.data.count;
          //console.log(data.data.count);
          //getboarddata();
          return lazyLoadExportData();
        }
      })

    }


    // csv export step 4 of 5
    function lazyLoadExportData(){

      promises = [];
      vm.objarray = [];
      for(var i=0; i < Math.ceil(vm.totalallrecord/500); i++) {
        promises.push(getbsddata(i*500));
      }

      return $q.all(promises).then(function(){


       var sortedCsvData = _.sortBy(vm.objarray, function(o) { return o.fName + o.lName; })

        sortedCsvData.unshift(['First Name', 'Last Name','Zip', 'Email','Allowed to receive Emails','Local / Regional']);
        vm.dataLoading = false;

        return sortedCsvData;
      });
    }

    // csv export step 5 of 5
    var getbsddata = function(skiprec){

      var d = $q.defer();
      return apiomahaService.getusersdatacsv(skiprec,500).then(function(data) {

        if(data.data && data.data.results.length >= 0){

          var csvdata = _.map(data.data.results, function(currentObject) {

            //var dsessiondatetime="";
            //if (currentObject.SessionDataTime != null)
            //{
            //  var dsessiondatetime = new Date(currentObject.SessionDataTime.iso);
            //}
            //var sessiondobj = new Object();
            //sessiondobj.sessiondatetime = dsessiondatetime;
            var reto =  _.pick(currentObject, 'fName','lName','zip','email','allowedFutureEmails','zipCodeType');
            //console.log('1111111111');
            //console.log(reto);
            //console.log('2222222222');
            //vm.objarray.push(_.extend(reto, sessiondobj));

            vm.objarray.push(reto);
            //console.log('obj array length');
            //console.log(vm.objarray.length);
            return reto;
          });


          d.resolve();
          return d.promise;
        }

      });


      // return d.promise;
    };





    // csv export step 1

    function offerreddatacsv()
    {
      vm.dataLoading = true;
      return gettotalofferredrecords();


    }


    // csv export step 2 of 5
    // csv export step 3 of 5
    function gettotalofferredrecords()
    {

      return apiomahaService.gettotalrecords('redeemedOffers').then(function(data) {

        if(data.data.count >= 0){
          vm.totalallrecord = data.data.count;
          //console.log(data.data.count);
          //getboarddata();
          return lazyLoadExportofferredData();
        }
      })

    }


    // csv export step 4 of 5
    function lazyLoadExportofferredData(){

      promises = [];
      vm.objarray = [];
      for(var i=0; i < Math.ceil(vm.totalallrecord/500); i++) {
        promises.push(getofferreddata(i*500));
      }

      return $q.all(promises).then(function(){

        //console.log('2222222');
        //console.log(vm.objarray);

        var sortedCsvData = _.sortBy(vm.objarray, function(o) { return o.name; });

        sortedCsvData.unshift(['Partner Name','Offer Title','First Name','Last Name','Zip Code','Email Address','Redemption Quantity','Redemption Date','Redemption Time']);
        vm.dataLoading = false;

        return sortedCsvData;
      });
    }



    // csv export step 5 of 5
    var getofferreddata = function(skiprec){

      var d = $q.defer();
      return apiomahaService.getofferredDatacsv(skiprec,500).then(function(data) {

        if(data.data && data.data.results.length >= 0){





          var csvdata = _.map(data.data.results, function(currentObject) {


            var createddateobj = new Object();
            var vdate = new Date(currentObject.createdAt);
            createddateobj.createddate = vdate.toLocaleDateString();
            createddateobj.createdtime = vdate.toTimeString();



            var filteredpartnerobj = new Object();
            if (currentObject.partner != null){
              var partnerobj = _.flatten(_.pick(currentObject, 'partner'));
              filteredpartnerobj.name = partnerobj[0].name;
            }


            var filteredofferobj = new Object();
            if (currentObject.offer != null) {
              var offerobj = _.flatten(_.pick(currentObject, 'offer'));
              filteredofferobj.title = offerobj[0].title;
              //filteredpartnerobj = _.pick(offerobj[0],'title');
            }


            var filteredUser = new Object();
            if (currentObject.user != null) {
              var userobj = _.flatten(_.pick(currentObject, 'user'));
              filteredUser = _.pick(userobj[0],'fName','lName','zip','email');
            }


            //'Partner Name','Offer Title','User Zip Code','User Email Address','Redemption Date','Redemption Quantity'


            var reto =  _.pick(currentObject,'qty');
            vm.objarray.push(_.extend(filteredpartnerobj,filteredofferobj, filteredUser,reto,createddateobj));


            return reto;
          });


          d.resolve();
          return d.promise;
        }

      });


      // return d.promise;
    };















  });
