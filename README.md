# omaha-main

This project is generated with [yo cg-angular].

## Build & development

Run `grunt build` for building and `grunt serve` for preview and development.

## Testing

Running `grunt test` will run the unit tests with karma.


## Coding Conventions

Please follow the link below for coding conventions.
https://github.com/johnpapa/angular-styleguide

