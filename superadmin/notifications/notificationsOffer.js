/**
 * Created by user on 4/4/16.
 */
angular.module('superadmin').controller('notificationCtrl', function (userService,$location,$cookies,$uibModal,$state,apiomahaService,$q,$route,NgTableParams,$scope) {

    var vm = this;

    vm.createSpecificNotificaiton = pushNotificaitonToSpecificuser;
    vm.createNotificaitonToAll = pushNotificaitonToAllUser;
    vm.recieverId = "";
    vm.alertMsgtoSpecficUser = "";
    vm.alertMsgtoAllUsers = "";

    init();

    function init(){

    }

    function pushNotificaitonToSpecificuser(){
      console.log('Inside createNotificationdata function');
      //{"receiverId":"sreekanthm@moback.com","data":{"alert":"Hello"}}
      var createnotificationdata = {
        "receiverId": vm.recieverId,
        "data":{
          "alert": vm.alertMsgtoSpecficUser
        }
      }

      userService.createNotificationdata(createnotificationdata).then(function(response){
        console.log('Inside createNotificaiton', response);

        if (response.data.code){
          window.alert('Notification created successfully!');
          window.location.reload();
        }
        $state.go('superadmin.notifications');
        if(response.data){
          $state.go('superadmin.notifications');
        }
      }, function(response){
        if(response.data && response.data.message){
          window.alert(response.data.message.message);
        }
      });
    }

    function pushNotificaitonToAllUser(){
      console.log('Inside pushNotificaitonToAllUser function');
      //{"receiverId":"sreekanthm@moback.com","data":{"alert":"Hello"}}
      var createnotificationdata = {
        "channels": [
          "__default"
        ],
        "data":{
          "alert": vm.alertMsgtoAllUsers
        }
      }

      userService.createNotificationdata(createnotificationdata).then(function(response){
        console.log('Inside createNotificaiton for all', response);

        if (response.data.code){
          window.alert('Notification created successfully!');
          window.location.reload();
        }
        $state.go('superadmin.notifications');
        if(response.data){
          $state.go('superadmin.notifications');
        }
      }, function(response){
        if(response.data && response.data.message){
          window.alert(response.data.message.message);
        }
      });
    }
  });
