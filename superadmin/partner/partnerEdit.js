/**
 * Created by user on 3/21/16.
 */
/**
 * Created by user on 3/21/16.
 */
/**
 * Created by user on 11/16/15.
 */
angular.module('superadmin').controller('partnerEditCtrl', function (userService,$location,$cookies,$uibModal,apiomahaService,$state,$stateParams,Upload, $http,$scope,endpoints) {

  var vm = this;
  vm.submitUpdateForm = submitEditForm;
  vm.cancel = cancelForm;
  vm.detectionRadius = 1;
  vm.partnerlogourl = "";
  vm.partnerlogname = "";
  vm.partnermapurl = "";
  vm.partnermapname = "";
  vm.location = "";
  vm.emails = "";
  vm.orgemails="";
  vm.checkOfferImage = false;
  vm.checkStreamImage = false;
  vm.showoLogoImageerror = false;
  vm.showMapImageError = false;
  $scope.isReadyToUpdate = true;

  init();

  function init(){
    vm.partnerid = $stateParams.partnerid;

    userService.getUserByadmin(vm.partnerid).then(function(response){
      console.log("User Info", response.data);
      if(response.data.results.length > 0){

        var partneremails = _.pluck(response.data.results, 'userId').join();
        vm.emails = partneremails;
        vm.orgemails = partneremails;
      }
    });



    apiomahaService.getpartner(vm.partnerid).then(function(response){

      console.log(response.data);
      if(response.data){
        console.log("getpartner", response.data);

        vm.objectId = response.data.objectId;
        vm.address1 = response.data.address1;
        vm.address2 = response.data.address2;
        vm.emails = response.data.emails;
        vm.city = response.data.city;
        vm.zip = response.data.zip;
        vm.phone = response.data.phone;
        vm.detectionRadius = response.data.detectionRadius;
        //vm.detectionRadius = response.data.radius;
        vm.name = response.data.name;
        vm.partnerlogourl = response.data.logo.url;
        vm.partnerlogname = response.data.logo.name;
        vm.partnermapurl = response.data.mapImage.url;
        vm.partnermapname = response.data.mapImage.url;
      }
    });

  }






  function submitEditForm(isValid) {
    

    console.log("vm.detectionRadius", vm.detectionRadius);
    if(vm.detectionRadius % 1 !=0){
      console.log("came inside to check detection value");
      window.alert("Please enter a valid integer value for Detection Radius");
      return;
    }
  
    if(vm.zip == undefined){
      console.log("came inside....");
      return;
    }
    if (isValid && !vm.showoLogoImageerror && !vm.showMapImageError){
      var logoobj = new Object();
      logoobj.__type = 'File';
      logoobj.name = vm.partnerlogname;
      logoobj.url = vm.partnerlogourl;
      vm.logo = logoobj;


      var mapImageoobj = new Object();
      mapImageoobj.__type = 'File';
      mapImageoobj.name =  vm.partnermapname;
      mapImageoobj.url = vm.partnermapurl;
      vm.mapImage = mapImageoobj;


      $http.get(endpoints.googleaddressapi + vm.address1+ ','+vm.city + ','+'NE' +','+ vm.zip).success(function(mapData) {

        if (mapData.results.length > 0){
          var locationobj = new Object();
          locationobj.__type = 'GeoPoint';
          locationobj.lat = mapData.results[0].geometry.location.lat;
          locationobj.lon = mapData.results[0].geometry.location.lng;
          vm.location = locationobj;
        }



        var createpartnerdata = _.pick(vm,'name','address1','address2','city','zip','phone','detectionRadius','emails','mapImage','logo','location');

        apiomahaService.updatepartner(vm.partnerid,createpartnerdata).then(function(response){


          userService.deletePartnerId(vm.orgemails).then(function(responsep){




            userService.assignPartnerId(vm.partnerid,vm.emails).then(function(responsec){
              console.log('assigned partner id to user..... ');
            }, function(response){
              if(response.data && response.data.message){
                console.log(response.data.message.message);
              }
            });



          }, function(response){
            if(response.data && response.data.message){
              console.log(response.data.message.message);
            }
          });




          window.alert('Partner successfully updated');
          //console.log(response.data);


          //if (response.data.success){
          //  window.alert('Partner successfully updated');
          //}
          //console.log(response.data);
          //window.alert(response.data.message);

          $state.go('superadmin.partner',{}, {reload: true});
          if(response.data && response.data.id){
            //var id = response.data.id;
            $state.go('superadmin.partner',{}, {reload: true});
          }
        }, function(response){
          console.log('Error:',response.data.message);
          if(response.data && response.data.message){
            window.alert(response.data.message);
          }
        });


      });



    }else{
      console.log('Invalid form...');
    }


/*


    var createpartnerdata = _.pick(vm,'name','address1','address2','city','zip','phone','detectionRadius');
    apiomahaService.updatepartner(vm.partnerid,createpartnerdata).then(function(response){

      //
        window.alert('Partner successfully updated');
      //}
      //console.log(response.data);
      //window.alert(response.data.message);

      $state.go('superadmin.partner');
      if(response.data && response.data.id){
        //var id = response.data.id;
        $state.go('superadmin.partner');
      }
    }, function(response){
      if(response.data && response.data.message){
        window.alert(response.data.message.message);
      }
    });

*/


  }

  function cancelForm() {
    //$uibModalInstance.dismiss('cancel');
    console.log('cancel..');
    //$state.go('superadmin.partner');
  };

vm.processImage = function(file, type){
  //logo Image
  if (type == 'LOGO') {
        var logoMaxImageWidth = 1066, logoMaxImageHeight = 497;
        var imgWidth, imgHeight;
        var _URL = window.URL || window.webkitURL;
        var img = new Image();
        img.onload = function() {
            imgWidth  = this.width;
            imgHeight = this.height;
            if (imgWidth === logoMaxImageWidth && imgHeight === logoMaxImageHeight) {
              console.log("valid dimension");
              vm.showoLogoImageerror = false;
              $scope.isReadyToUpdate = false;
              uploadFiles(file);
            }else{
              vm.showoLogoImageerror = true;
              console.log("invalid dimension");
              return;
            }
        };
        img.onerror = function() {
            console.log( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
  }
  //map Image
  if (type == 'MAP') {
        var mapMaxImageWidth = 1280, mapMaxImageHeight = 620;
        var imgWidth, imgHeight;
        var _URL = window.URL || window.webkitURL;
        var img = new Image();
        img.onload = function() {
            imgWidth  = this.width;
            imgHeight = this.height;
            if (imgWidth === mapMaxImageWidth && imgHeight === mapMaxImageHeight) {
              console.log("valid stream image dimension");
              $scope.isReadyToUpdate = false;
              vm.showMapImageError = false;
              uploadFilesmap(file);
            }else{
              console.log("invalid stream image dimension");
              vm.showMapImageError = true;
              return;
            }
        };
        img.onerror = function() {
            console.log( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
  }
  

}

    var uploadFiles = function(file) {
    vm.f = file;
    //vm.errFile = errFiles && errFiles[0];
    if (file) {
      if(vm.partnerlogourl){
        console.log("check partnerlogourl values");
        vm.checkOfferImage = false;
      }

      var fd = new FormData();
      fd.append('file', file);
      $http.put(endpoints.MobackFileManager, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined,'X-Moback-Application-Key': endpoints.MobackApplicationKey}
        })
        .success(function(data){

          console.log(data);

          if (data.code == '1000')
          {
            //alert(data.url);
            vm.partnerlogourl = data.url;
            vm.partnerlogname = data.name;
            vm.showoLogoImageerror = false;
            $scope.isReadyToUpdate = false;
          }

        })
        .error(function(data){
          //alert('error'+data);
          console.log('error'+data);
        });



    }
  }




  var uploadFilesmap = function(file) {


    vm.f = file;
    //vm.errFile = errFiles && errFiles[0];
    if (file) {

      if(vm.partnermapurl){
        console.log("check partnermapurl values");
        vm.checkStreamImage = false;
      }

      var fd = new FormData();
      fd.append('file', file);
      $http.put(endpoints.MobackFileManager, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined,'X-Moback-Application-Key': endpoints.MobackApplicationKey}
        })
        .success(function(data){
          //console.log(data);

          if (data.code == '1000')
          {
            $scope.isReadyToUpdate = false;
            vm.showMapImageError = false;
            //alert(data.url);
            vm.partnermapurl = data.url;
            vm.partnermapname = data.name;

          }

        })
        .error(function(data){
          //alert('error'+data);
          console.log('error'+data);
        });



    }
  }

});
