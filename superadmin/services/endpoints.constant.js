/**
 * Created by bhavesh on 11/4/2015.
 */
/**
 * @ngdoc service
 * @name forteInternalMobilityApp.endpoints
 * @description
 * # endpoints
 * Constant in the forteInternalMobilityApp.
 */
angular.module('superadmin')
  .constant('endpoints', {

    /*Mobackuser: 'https://api.moback.com/usermanager/api/users/',
    MobackFileManager: 'https://api.moback.com/filemanager/api/files/upload',
    Mobackcollection: 'https://api.moback.com/objectmgr/api/collections/',
    MobackSingleUser: 'https://api.moback.com/usermanager/api/user/',*/

    Mobackuser: 'http://34.234.62.192:8080/omaha/usermanager/api/users/',
    MobackFileManager: 'http://34.234.62.192:8080/omaha/filemanager/api/files/upload',
    Mobackcollection: 'http://34.234.62.192:8080/omaha/objectmgr/api/collections/',
    MobackSingleUser: 'http://34.234.62.192:8080/omaha/usermanager/api/user/',

    /* bhavesh's moback account*/
    /*
    MobackApplicationKey : 'YmU0NjJmMmUtOTE4Ni00ZmY3LWJjNWMtZTc2Y2Y4NGMyMmM2',
    MobackEnvironmentKey : 'MmMwMTMzZTktOTdmNC00ZTQ2LTk1ZmMtZjQzMWMzZWQwNGZm',
     */
    MobackApplicationKey : 'YWU5MmY2NDAtNjBhOS00ZmE3LTg4ZjUtNTEyMTVkZGE3NGU0',
    MobackEnvironmentKey : 'NzcyMzk4NzAtN2QzOS00Y2M3LTlkODAtNDAyY2ZiM2U5ZTE3',

    googleKeyName : 'key1',
    googleApiKey : 'AIzaSyBjx6zAxulwI6bV8kXgnXExKRaBAAf0uvo',
    googlegeocodingurl : 'http://maps.googleapis.com/maps/api/geocode/output?parameters',
    googleaddressapi : 'http://maps.google.com/maps/api/geocode/json?sensor=false&address=',

    alertNotifications : 'http://34.234.62.192:8080/omaha/notificationmanager/api/alerts/push'

  });
