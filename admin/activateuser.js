/**
 * Created by user on 6/7/16.
 */
angular.module('admin').controller('activateuserController', function(userService, $state, $cookies,$stateParams){

  var vm = this;

  vm.actoken;
  vm.message="";

  init();

  function init(){

    vm.actoken = $stateParams.tokenid;
    //activateAppUserPasswordAccount

    userService.activateAppUserPasswordAccount(vm.actoken).then(function(data){
      //$location.path( '/editProfile' );
      //console.log('login controller');
      //console.log(data.data);
      //vm.message = data.data.message;
      //console.log('activation message');
      //console.log(data.data.code);
      if (data.data.code == 1000)
      {
        //console.log('activation message end');
        vm.message = 'Your Omaha Savings account has been verified.';
        var messagetext = 'Your Omaha Savings account has been verified.';
        var messageheader = 'CONGRATS!';
        $state.go('admin.custommessages',{ "messageheader": messageheader , "messagetext":messagetext});
      }
      else
      {
        vm.message = data.data.message;

        var messagetext = data.data.message;
        var messageheader = 'ERROR!';
        $state.go('admin.custommessages',{ "messageheader": messageheader , "messagetext":messagetext});

      }



    }, function(data){
       alert('dd');
      if(data.data && data.data.message){
        //window.alert(data.data.message);
      }
    });

  }

});
