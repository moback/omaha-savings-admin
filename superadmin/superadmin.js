angular.module('superadmin', ['ui.bootstrap', 'ui.utils', 'ui.router', 'ngAnimate', 'ngRoute',
  "isteven-multi-select", 'ngTable','ngFileUpload','smart-table','ngSanitize','ngCsv']);


angular.module('superadmin').config(function($stateProvider) {

  /* Add New States Above */

  $stateProvider.state('superadmin', {
    url: '/superadmin',
    views: {
      "": {
        templateUrl: 'superadmin/superadmin.html'
      },
      "viewHeader@superadmin": {
        templateUrl: 'superadmin/header.html',
        controller: 'superadminHeaderController',
        controllerAs: 'vm'
      },
      "breadcrumb@superadmin": {
        templateUrl: 'superadmin/breadcrumb.html',
        controller: 'superadminBreadcrumbController',
        controllerAs: 'vm'
      },
      "@superadmin": {
        templateUrl: 'superadmin/announcement.html',
        controller: 'announcementCtrl',
        controllerAs: 'vm'
      }
    }
  }).state('superadmin.partner', {
    url: '/partner',
    views : {
      "" : {
        templateUrl : 'superadmin/partner/layout.html'
      },
      "viewTopBar@superadmin.partner" : {
        templateUrl : 'superadmin/partner/partner_topbar.html',
        //controller : 'bookTopNavController',
        ///controllerAs: 'vm'
      },
      "@superadmin.partner" : {
        templateUrl : 'superadmin/partner/partnerList.html',
        controller: 'partnerListCtrl',
        controllerAs: 'vm'
      }
    }
  }).state('superadmin.announcement', {
    url: '/announcement',
    views: {
      "@superadmin.announcement": {
        templateUrl: 'superadmin/announcement.html',
        controller: 'announcementCtrl',
        controllerAs: 'vm',
        title:'announcement'
      }
    }
  }).state('superadmin.partner.upload', {
    url: '/partnerupload',
    views: {
      "@superadmin.partner": {
        templateUrl: 'superadmin/partner/partnerCreate.html',
        controller: 'partnerCreateCtrl',
        controllerAs: 'vm'
      }
    }
  }).state('superadmin.partner.edit', {
    url: '/partneredit/:partnerid',
    views: {
      "@superadmin.partner": {
        templateUrl: 'superadmin/partner/partnerEdit.html',
        controller: 'partnerEditCtrl',
        controllerAs: 'vm'
      }
    }
  }).state('superadmin.report', {
    url: '/report',
    views : {
      "" : {
        templateUrl : 'superadmin/reports/layout.html'
      },
      "viewTopBar@superadmin.report" : {
        templateUrl : 'superadmin/reports/reports_topbar.html',
        //controller : 'TopNavController',
        ///controllerAs: 'vm'
      },
      "@superadmin.report" : {
        templateUrl : 'superadmin/reports/offerReport.html',
        controller: 'offerReportCtrl',
        controllerAs: 'vm'
      }
    }
  }).state('superadmin.notifications', {
    url: '/notifications',
    views : {
      "" : {
        templateUrl : 'superadmin/notifications/layout.html'
      },
      "viewTopBar@superadmin.notifications" : {
        templateUrl : 'superadmin/notifications/notifications_topbar.html',
      },
      "@superadmin.notifications" : {
        templateUrl : 'superadmin/notifications/notifications_offer.html',
        controller: 'notificationCtrl',
        controllerAs: 'vm'
      }
    }
  })
  .state('superadmin.offer', {
    url: '/offer',
    views : {
      "" : {
        templateUrl : 'superadmin/offer/layout.html'
      },
      "viewTopBar@superadmin.partner" : {
        templateUrl : 'superadmin/offer/offer_topbar.html',
        //controller : 'TopNavController',
        ///controllerAs: 'vm'
      },
      "@superadmin.offer" : {
        templateUrl : 'superadmin/offer/offerList.html',
        controller: 'offerListCtrl',
        controllerAs: 'vm',
        title:'Offer List'
      }
    }
  }).state('superadmin.user', {
    url: '/user',
    views : {
      "" : {
        templateUrl : 'superadmin/user/layout.html'
      },
      "viewTopBar@superadmin.user" : {
        templateUrl : 'superadmin/user/user_topbar.html',
        //controller : 'TopNavController',
        ///controllerAs: 'vm'
      },
      "@superadmin.user" : {
        templateUrl : 'superadmin/user/userList.html',
        controller: 'userListCtrl',
        controllerAs: 'vm',
        title:'User List'
      }
    }
  }).state('superadmin.offer.upload', {
    url: '/offerupload',
    views: {
      "@superadmin.offer": {
        templateUrl: 'superadmin/offer/offerCreate.html',
        controller: 'offerCreateCtrl',
        controllerAs: 'vm',
        title:'Create Offer'
      }
    }
  }).state('superadmin.offer.edit', {
      url: '/offeredit/:offerid',
      views: {
        "@superadmin.offer": {
          templateUrl: 'superadmin/offer/offerEdit.html',
          controller: 'offerEditCtrl',
          controllerAs: 'vm',
          title:'Update Offer'
        }
      }
    })
    .state('superadmin.profile', {
      url: '/profile',
      views: {
        "": {
          templateUrl: 'superadmin/superadminProfile.html',
          //controller: 'superadminProfileCtrl'
        }

      }

    });

});
