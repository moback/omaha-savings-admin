'use strict';

angular.module('superadmin')
    .controller('superadminBreadcrumbController', function($scope, $location) {

        var vm = this;
        vm.getBreadCrumb = getBreadCrumb;

        function getBreadCrumb() {
            var path = $location.path();
            var breadCrumb = '';

            switch (path) {
                case '/superadmin':
                    breadCrumb = "Home";
                    break;
                case '/superadmin/announcements':
                    breadCrumb = "Home";
                    break;


                default:
            }
            return breadCrumb;
        }

    });
