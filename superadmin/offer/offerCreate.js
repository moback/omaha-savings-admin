/**
 * Created by user on 3/22/16.
 */
/**
 * Created by user on 3/22/16.
 */
/**
 * Created by user on 3/21/16.
 */
/**
 * Created by user on 11/16/15.
 */
angular.module('superadmin').controller('offerCreateCtrl', function (userService,$location,$cookies,$uibModal,apiomahaService,$state,$scope,$http,endpoints,$stateParams) {

  var vm = this;
  vm.allpartnerlist = [];
  vm.submitOfferForm = submitOfferForm;
  vm.cancel = cancelForm;

  vm.offerType = ['regional','local','both'];
  vm.type = vm.offerType[0];
  $scope.mindate = new Date();
  vm.startDateTime = new Date();
  vm.actstartdatePickerIsOpen = false;
  vm.barCodeurl = "";
  vm.barCodeName = "";
  vm.offerImageurl = "";
  vm.offerImageName = "";
  vm.streamImageurl = "";
  vm.streamImageName = "";
  vm.minParticipants = 1;
  vm.maxParticipants = 1;
  vm.showofferimageerror = false;
  vm.showofferImageError = false;
  vm.showStreamImageError = false;
  vm.showBarcodeImageError = false;
  vm.checkOfferImage = false;
  vm.checkStreamImage = false;
  

  init();


  function init() {

    vm.partnerid = $stateParams.partnerid;

    getpartnerlist();
  }



  $scope.$watch(function () {
    return vm.actstartdatePickerIsOpen;
  },function(value){
    vm.opens.push("actstartdatePickerIsOpen: " + value + " at: " + new Date());
  });


  vm.actstartdatePickerOpen = function ($event) {

    if ($event) {
      $event.preventDefault();
      $event.stopPropagation();
    }
    this.actstartdatePickerIsOpen = true;
  };


  //vm.expirationDateTime = new Date(Date.now() + 864e5 * 30);
  vm.expirationDateTime = new Date();
  vm.actenddatePickerIsOpen = false;
  vm.opens = [];

  $scope.$watch(function () {
    return vm.actenddatePickerIsOpen;
  },function(value){
    vm.opens.push("actenddatePickerIsOpen: " + value + " at: " + new Date());
  });

  vm.actenddatePickerOpen = function ($event) {

    if ($event) {
      $event.preventDefault();
      $event.stopPropagation();
    }
    this.actenddatePickerIsOpen = true;
  };



  function getpartnerlist(){
    apiomahaService.getpartnerlist().then(function(data){

      if(data.data && data.data.results.length >= 0){
        //console.log('aaaaaaaaaa');
        //console.log(data.data.results);
        //console.log('bbbbbbbbb');

        vm.allpartnerlist = _.sortBy(data.data.results, 'name');
        vm.selectedpartner = vm.allpartnerlist[0];

      }
    });
  }

  var toUTCDate = function(date){
    var _utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  23, 1, 1 );
    return _utc.toISOString();
  };


  function submitOfferForm(isValid) {
    if (isValid && !vm.showofferimageerror && !vm.showStreamImageError && !vm.showBarcodeImageError) {
      console.log("form is amazing", vm.showofferimageerror, vm.showStreamImageError ,vm.showBarcodeImageError);
    }else{

      console.log("form is not valid", isValid,vm.showofferimageerror, vm.showStreamImageError ,vm.showBarcodeImageError);
      return;
    }
    console.log("offer photo",vm.teaserPhoto);
    console.log("offer stream photo",vm.streamPhoto);
    if(vm.minParticipants > vm.maxParticipants){
      window.alert("Mininum no. of participants can't be more than Maximum no. participants");
      return;
    }
    if(vm.expirationDateTime < vm.startDateTime){
      console.log("date range issue");
      window.alert("Expiration date can't be less than Start date");
      return;
    }else{
      if (isValid && !vm.showofferimageerror && !vm.showStreamImageError && !vm.showBarcodeImageError){

        if (vm.type == 'both'){
          vm.type = 'regional,local';
        }
        //  vm.partnerPoint = vm.selectedpartner.objectId;
  
  var endDate = new Date(
      vm.expirationDateTime.getFullYear(), vm.expirationDateTime.getMonth(), vm.expirationDateTime.getDate(),
      23, 0, 0 //last hour of the day
    );
        

        var startDateTimeobj = new Object();
        startDateTimeobj.__type = 'Date';
        startDateTimeobj.iso = vm.startDateTime.toISOString();
        vm.startDateTime = startDateTimeobj;
  
        var expDateTimeobj = new Object();
        expDateTimeobj.__type = 'Date';
        expDateTimeobj.iso = endDate;
        vm.expirationDateTime = expDateTimeobj;

        
  
        var barcodeImageobj = new Object();
        barcodeImageobj.__type = 'File';
        barcodeImageobj.name = vm.barCodeName;
        barcodeImageobj.url = vm.barCodeurl;
        vm.barcodeImage = barcodeImageobj;
  
        var teaserPhotoobj = new Object();
        teaserPhotoobj.__type = 'File';
        teaserPhotoobj.name = vm.offerImageName;
        teaserPhotoobj.url = vm.offerImageurl;
        vm.teaserPhoto = teaserPhotoobj;
  
        var streamPhotoobj = new Object();
        streamPhotoobj.__type = 'File';
        streamPhotoobj.name = vm.streamImageName;
        streamPhotoobj.url = vm.streamImageurl;
        vm.streamPhoto = streamPhotoobj;
  
  
        var partnerobj = new Object();
        partnerobj.__type = 'Pointer';
        partnerobj.className = 'partners';
        partnerobj.objectId = vm.selectedpartner.objectId;
        vm.partnerPoint = partnerobj;
  
  
        vm.active = true;
        vm.isFeatured = false;
  
        //expirationDateTime: {__type: "Date", iso: "2016-03-06T02:31:58.000Z"}
        var createofferdata = _.pick(vm,'type','title','description','restrictions','minParticipants','maxParticipants','isFeatured','startDateTime','expirationDateTime','barcodeImage','teaserPhoto','streamPhoto','partnerPoint','active','isFeatured');
  
        //var partnerobj = new Object();
        //partnerobj.partnerPoint = vm.selectedpartner.objectId;
        //var createofferdata  = _.extend(createofferdata,partnerobj);
        console.log("createofferdata", createofferdata);
        console.log("came inside stream photo section", vm.streamPhoto);
        // if(vm.streamPhoto.url == ""){
        //  console.log("came inside stream photo section1");
        //   return;
        // }
        apiomahaService.createmobackdata('offers',createofferdata).then(function(response){
          //console.log('inside offers', response);
  
          if (response.data.results.success){
            window.alert('Offer successfully created');
          }
  
          //console.log(response.data);
          //window.alert(response.data.message);
  
          $state.go('superadmin.offer');
          if(response.data && response.data.id){
            //var id = response.data.id;
            $state.go('superadmin.offer');
          }
        }, function(response){
          if(response.data && response.data.message){
            window.alert(response.data.message.message);
          }
        });
      }else{
  
        console.log("Something went wrong.");
      }
    }

  }




  function cancelForm() {
    //$uibModalInstance.dismiss('cancel');
    $state.go('superadmin.offer');
  }
vm.processImage = function(file, type){
  //Offer Image
  if (type == 'OFFER') {
        var offerMaxImageWidth = 1280, offerMaxImageHeight = 1031;
        var imgWidth, imgHeight;
        var _URL = window.URL || window.webkitURL;
        var img = new Image();
        img.onload = function() {
            imgWidth  = this.width;
            imgHeight = this.height;
            if (imgWidth === offerMaxImageWidth && imgHeight === offerMaxImageHeight) {
              console.log("valid dimension");
              vm.showofferimageerror = false;
              uploadOfferFiles(file);
            }else{
              vm.showofferimageerror = true;
              console.log("invalid dimension");
              return;
            }
        };
        img.onerror = function() {
            console.log( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
  }
  //Offer Image
  if (type == 'STREAM') {
        var streamMaxImageWidth = 1280, streamMaxImageHeight = 640;
        var imgWidth, imgHeight;
        var _URL = window.URL || window.webkitURL;
        var img = new Image();
        img.onload = function() {
            imgWidth  = this.width;
            imgHeight = this.height;
            if (imgWidth === streamMaxImageWidth && imgHeight === streamMaxImageHeight) {
              console.log("valid stream image dimension");
              vm.showStreamImageError = false;
              uploadstreamFiles(file);
            }else{
              console.log("invalid stream image dimension");
              vm.showStreamImageError = true;
              return;
            }
        };
        img.onerror = function() {
            console.log( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
  }
  //barcode image
  if (type == 'BARCODE') {
        var barcodeMaxImageWidth = 915, barcodeMaxImageHeight = 225;
        var imgWidth, imgHeight;
        var _URL = window.URL || window.webkitURL;
        var img = new Image();
        img.onload = function() {
            imgWidth  = this.width;
            imgHeight = this.height;
            if (imgWidth === barcodeMaxImageWidth && imgHeight === barcodeMaxImageHeight) {
              console.log("valid barcode image dimension");
              vm.showBarcodeImageError = false;
              uploadBarcodeFiles(file);
            }else{
              console.log("invalid barcode image dimension");
              vm.showBarcodeImageError = true;
              return;
            }
        };
        img.onerror = function() {
            console.log( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
  }

}

  var uploadOfferFiles = function(file) {

    //vm.f = file;
    //vm.errFile = errFiles && errFiles[0];
    if (file) {

        




      if(vm.teaserPhoto){
        console.log("check teaserPhoto values");
        vm.checkOfferImage = false;
      }
       var fd = new FormData();
       fd.append('file', file);
       $http.put(endpoints.MobackFileManager, fd, {
       transformRequest: angular.identity,
       headers: {'Content-Type': undefined,'X-Moback-Application-Key': endpoints.MobackApplicationKey}
       })
       .success(function(data){
       //console.log(data);

       if (data.code == '1000')
       {
         vm.showofferimageerror = false;
         vm.showofferImageError = false;

       vm.offerImageurl = data.url;
       vm.offerImageName = data.name;
       console.log("vm.offerImageurl", vm.offerImageurl, "vm.offerImageName", vm.offerImageName);

       }

       })
       .error(function(data){
       //alert('error'+data);
       console.log('error'+data);
       });

    }
  };



  var uploadstreamFiles = function(file) {
    //vm.f = file;
    //vm.errFile = errFiles && errFiles[0];
    if (file) {

      if(vm.streamPhoto){
        console.log("check streamPhoto values");
        vm.checkStreamImage = false;
      }

      var fd = new FormData();
      fd.append('file', file);
      $http.put(endpoints.MobackFileManager, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined,'X-Moback-Application-Key': endpoints.MobackApplicationKey}
        })
        .success(function(data){
          //console.log(data);

          if (data.code == '1000')
          {
            vm.showstreamimageerror = false;

            vm.streamImageurl = data.url;
            vm.streamImageName = data.name;

            console.log("vm.streamImageurl", vm.streamImageurl, "vm.streamImageName", vm.streamImageName);

          }

        })
        .error(function(data){
          //alert('error'+data);
          console.log('error'+data);
        });

    }
  };




  var uploadBarcodeFiles = function(file) {


    //vm.f = file;
    //vm.errFile = errFiles && errFiles[0];
    if (file) {

     // alert('barcode');

      var fd = new FormData();
      fd.append('file', file);
      $http.put(endpoints.MobackFileManager, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined,'X-Moback-Application-Key': endpoints.MobackApplicationKey}
        })
        .success(function(data){

          if (data.code == '1000')
          {
            //alert(data.url);
            vm.barCodeurl = data.url;
            vm.barCodeName = data.name;
          }

        })
        .error(function(data){
          //alert('error'+data);
          console.log('error'+data);
        });




    }
  };













});
