/**
 * Created by user on 3/21/16.
 */
angular.module('superadmin')
  .controller('partnerListCtrl', function (userService,$location,$cookies,$uibModal,$state,apiomahaService,$q,$route) {

    var vm = this;
    vm.allpartnerlist = [];
    vm.createRecord = createModal;
    vm.deleteRecord = deleteModal;
    vm.editRecord = editModal;
    vm.partneremaillist = [];
    //vm.gettexttype = gettexttypeModel;



    init();

    function init(){
      getpartnerlist();
    }



    // get all textMain
    function getpartnerlist(){


      apiomahaService.getpartnerlist().then(function(data){

        if(data.data && data.data.results.length >= 0){

          vm.allpartnerlist = data.data.results;
          console.log("vm.allpartnerlist", vm.allpartnerlist);

          apiomahaService.getpartneremaillist().then(function(data) {

            if (data.data && data.data.results.length >= 0) {



              vm.partneremaillist = data.data.results;

              var reducedpartemail = _.map(vm.partneremaillist, function(propvalue){
                            return {'partner':propvalue.admin.objectId,'email':propvalue.email};
              });

              _.each(vm.allpartnerlist, function(element, index) {

                var searchedemail = _.where(reducedpartemail,{partner:element.objectId});
                if (searchedemail.length > 0){
                  _.extend(element, {email: _.pluck(searchedemail,'email').toString()});
                }
                else
                {
                  _.extend(element, {email: ''});
                }
              });

            }
          });
        }
      });

    }


    function createModal() {

      $state.go('superadmin.partner.upload');
    }


    function editModal(partnerId) {
      //console.log(partnerId);
      $state.go('superadmin.partner.edit', {partnerid: partnerId});

    }


    function deleteModal(){
      var deleteModal = window.confirm("Are you sure you want to remove selected Partners?");






      if (deleteModal) {
        //console.log('inside delete');
        angular.toJson(vm.allpartnerlist);
        //console.log(vm.allbooklist);
        var deleteMethods = [];
        for (var i = 0; i < vm.allpartnerlist.length; i++) {
          if(vm.allpartnerlist[i].checked === true){
            //console.log(vm.allpartnerlist[i].objectId);
            deleteMethods.push(apiomahaService.deletepartner(vm.allpartnerlist[i].objectId));
            //window.location.reload();
           // $location.reload;
           // $state.go('superadmin.partner');
          }
        }

        if(deleteMethods.length > 0){
          //console.log(vm.allbooklist[i]);
          //fire all delete methods
          $q.all(deleteMethods).then(function(){
            window.alert('Partners successfully deleted');
            window.location.reload();
            //$location.reload;
            //$window.location.reload();
            //$state.go('superadmin.partner');
          });
          //$location.reload();

          //$state.go('superadmin.partner');
        }



      }


    }

  });
