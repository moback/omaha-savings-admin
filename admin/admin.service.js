/**
 * @ngdoc service
 * @name fortesuperadminApp.userService
 * @description
 * # userService
 * Service in the fortesuperadminApp.
 */
angular.module('admin')
  .service('adminService', function (endpoints, $cookies, $q, apiService) {



    this.getAdminProfile = function(userName){
      var req = {
        method: 'GET',
        url: endpoints.user + 'getAdmin/' + userName
      };

      return apiService.adminAPICall(req);
    };

    this.updateAdminProfile = function(userName, formData){
      var req = {
        method: 'POST',
        url: endpoints.user + 'AdminUpdate/' + userName,
        data: formData
      };

      return apiService.adminAPICall(req);
    };


    this.updateUserStatus = function(user, isEnabled){
      var toPost = {
        "enable": isEnabled,
        "userNames": [
          user.userName
        ]
      };
      var req = {
        method: 'POST',
        url: endpoints.user + 'updateUserStatus',
        data: toPost
      };

      return apiService.adminAPICall(req);
    };

  });
