/**
 * Created by user on 3/22/16.
 */
angular.module('superadmin')
  .controller('offerListCtrl', function (userService,$location,$cookies,$uibModal,$state,apiomahaService,$q,$route) {

    var vm = this;
    vm.allofferlist = [];
    vm.createRecord = createModal;
    vm.deleteRecord = deleteModal;
    vm.editRecord = editModal;

    //vm.gettexttype = gettexttypeModel;



    init();

    function init(){
      getofferlist();
    }



    // get all textMain
    function getofferlist(){
      apiomahaService.getofferlist().then(function(data){

        if(data.data && data.data.results.length >= 0){

          //vm.allofferlist = data.data.results;
          vm.allofferlist = _.map(data.data.results, function(currentObject) {
            var expireddate = new Date(currentObject.expirationDateTime.iso);
            var currentdate = new Date();
            var isexpired = expireddate < currentdate;
            currentObject["isexpired"]=isexpired;
            var finalusrobjlist =  _.pick(currentObject, 'objectId','partnerPoint','type','title','startDateTime','expirationDateTime','createdAt','isexpired');
            return finalusrobjlist;
          });

          //var uploadbookpartial = _.pluck(data.data,'book');
          //var uploadbookpartial1 = _.pluck(uploadbookpartial,'textType');
          //_.each(uploadbookpartial, function(element, index, list) {
          //idlist.push(element.id);
          // uploadbookpartial.aaa = index;
          //console.log(uploadbookpartial);
          //});
          //console.log(angular.toJson(uploadbookpartial));

        }
      });
    }


    function createModal() {

      $state.go('superadmin.offer.upload');
    }


    function editModal(pofferId) {
      console.log(pofferId);
      $state.go('superadmin.offer.edit', {offerid: pofferId});
    }




    function deleteModal(){
      var deleteModal = window.confirm("Are you sure you want to remove selected offers?");

      if (deleteModal) {
        //console.log('inside delete');
        angular.toJson(vm.allofferlist);
        //console.log(vm.allbooklist);
        var deleteMethods = [];
        for (var i = 0; i < vm.allofferlist.length; i++) {
          if(vm.allofferlist[i].checked === true){
            //console.log(vm.allpartnerlist[i].objectId);
            deleteMethods.push(apiomahaService.deleteoffer(vm.allofferlist[i].objectId));
            //window.location.reload();
            // $location.reload;
            // $state.go('superadmin.partner');
          }
        }

        if(deleteMethods.length > 0){
          //console.log(vm.allbooklist[i]);
          //fire all delete methods
          $q.all(deleteMethods).then(function(){
            window.alert('Offers successfully deleted');
            window.location.reload();
            //$location.reload;
            //$window.location.reload();
            //$state.go('superadmin.partner');
          });
          //$location.reload();

          //$state.go('superadmin.partner');
        }



      }


    }

  });
