/**
 * Created by user on 1/4/17.
 */
/**
 * Created by user on 3/22/16.
 */
angular.module('superadmin')
  .controller('userListCtrl', function (userService,$location,$cookies,$uibModal,$state,apiomahaService,userService,$q,$route,NgTableParams,$filter) {

    var vm = this;
    vm.userlist = [];
    vm.totalusers = 0;
    vm.disableusers = disableModal;
    vm.enableusers = enableModal;
    vm.applyGlobalSearch = cuatomuseridsearch;
    vm.searchuserterm = "";
    vm.dataLoading = false;
    vm.sendverification = sendverificationModel;
    init();



    function init(){
     getuserlist();
    }



    function cuatomuseridsearch(){
      vm.dataLoading = true;


      userService.getAllUsersCondCount(vm.searchuserterm).then(function(data){

        if(data.data.count){

          console.log(data.data.count);

            vm.userlistTable = new NgTableParams({
              page: 1,            // show first page
              count: 100,          // count per page
              sorting: {
                foo: 'asc'     // initial sorting
              }
            }, {
              total: data.data.count, // length of data
              getData: function($defer, params) {

                var page = params.page();
                var size = params.count();

                userService.getAllUsersSkipBatchCond(0,500,vm.searchuserterm).then(function(newdata) {

                  if (newdata.data && newdata.data.results.length >= 0) {
                    vm.userlist = newdata.data.results;
                    var orderedData = params.sorting() ?
                      $filter('orderBy')(vm.userlist, params.orderBy()) :
                      vm.userlist;
                    //$defer.resolve(orderedData);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                  }
                  vm.dataLoading = false
                });
              }
            });
        }

      });
      //vm.dataLoading = false;



    }


    // get all textMain
    function getuserlist(){

        apiomahaService.recordCount().then(function(data){

          if(data.data){


            vm.totalusers = data.data[5].count;

                vm.userlistTable = new NgTableParams({
                  page: 1,            // show first page
                  count: 100,          // count per page
                  sorting: {
                    foo: 'asc'     // initial sorting
                  }
                }, {
                  total: vm.totalusers, // length of data
                  getData: function($defer, params) {

                    var page = params.page();
                    var size = params.count();
                    var filter = params.filter().Email;


                    userService.getAllUsersSkipBatch((page - 1) * 100,100).then(function(newdata) {

                      if (newdata.data && newdata.data.results.length >= 0) {
                        vm.userlist = newdata.data.results;
                        var orderedData = params.sorting() ?
                          $filter('orderBy')(vm.userlist, params.orderBy()) :
                          vm.userlist;

                        $defer.resolve(orderedData);

                       // $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));


                      }
                    });

                  }


                });

          }



      });
      //


    }


    function createModal() {

      $state.go('superadmin.offer.upload');
    }


    function editModal(pofferId) {
      console.log(pofferId);
      $state.go('superadmin.offer.edit', {offerid: pofferId});
    }


    function enableModal(){

      var selecteditemslength = _.where(vm.userlist, {checked: true}).length;
      if (selecteditemslength > 0){
        var disableAns = window.confirm("Are you sure you want to enable selected users?");
      }
      else {
        window.confirm("Please select atleast one user");
      }


      if (disableAns) {
        console.log('inside disable');
        angular.toJson(vm.userlist);

        var disableuserlist = [];
        for (var i = 0; i < vm.userlist.length; i++) {
          if(vm.userlist[i].checked === true){
            //console.log(vm.allpartnerlist[i].objectId);
            disableuserlist.push(userService.enabledisableusers(vm.userlist[i].objectId,false));
            //window.location.reload();
            // $location.reload;
            // $state.go('superadmin.partner');
          }
        }
        console.log(disableuserlist);
        if(disableuserlist.length > 0){
          //console.log(vm.allbooklist[i]);
          //fire all delete methods
          $q.all(disableuserlist).then(function(){
            window.alert('Selected users successfully enabled');
            window.location.reload();
            //$location.reload;
            //$window.location.reload();
            //$state.go('superadmin.partner');
          });
          //$location.reload();

          //$state.go('superadmin.partner');
        }

      }


    }


    function disableModal(){

      var selecteditemslength = _.where(vm.userlist, {checked: true}).length;
      if (selecteditemslength > 0){
        var disableAns = window.confirm("Are you sure you want to disable selected users?");
      }
      else {
        window.confirm("Please select atleast one user");
      }


      if (disableAns) {
        console.log('inside disable');
        angular.toJson(vm.userlist);

        var disableuserlist = [];
        for (var i = 0; i < vm.userlist.length; i++) {
          if(vm.userlist[i].checked === true){
            //console.log(vm.allpartnerlist[i].objectId);
            disableuserlist.push(userService.enabledisableusers(vm.userlist[i].objectId,true));
            //window.location.reload();
            // $location.reload;
            // $state.go('superadmin.partner');
          }
        }
        console.log(disableuserlist);
        if(disableuserlist.length > 0){
          //console.log(vm.allbooklist[i]);
          //fire all delete methods
          $q.all(disableuserlist).then(function(){
            window.alert('Selected users successfully disabled');
            window.location.reload();
            //$location.reload;
            //$window.location.reload();
            //$state.go('superadmin.partner');
          });
          //$location.reload();

          //$state.go('superadmin.partner');
        }



      }


    }



    function sendverificationModel(){

      var selecteditemslength = _.where(vm.userlist, {checked: true}).length;
      if (selecteditemslength > 0){
        var disableAns = window.confirm("Are you sure you want to resend verification emails to selected users?");
      }
      else {
         window.confirm("Please select atleast one user");
      }

      if (disableAns) {
        console.log('inside disable');
        angular.toJson(vm.userlist);

        var sendveryfiuserlist = [];
        for (var i = 0; i < vm.userlist.length; i++) {
          if(vm.userlist[i].checked === true){


            console.log('inside loop');

            console.log(vm.userlist[i].email);
            sendveryfiuserlist.push(userService.resendVerification(vm.userlist[i].email));
            //window.location.reload();
            // $location.reload;
            // $state.go('superadmin.partner');
          }
        }
        console.log(sendveryfiuserlist);
        if(sendveryfiuserlist.length > 0){
          //console.log(vm.allbooklist[i]);
          //fire all delete methods
          $q.all(sendveryfiuserlist).then(function(){
            window.alert('Veryfication email has been sent successfully to Selected users');
            window.location.reload();
            //$location.reload;
            //$window.location.reload();
            //$state.go('superadmin.partner');
          });
          //$location.reload();

          //$state.go('superadmin.partner');
        }



      }


    }







  });
