angular.module('admin', ['ui.bootstrap','ui.utils','ui.router','ngAnimate', 'omahaService','superadmin']);

angular.module('admin').config(function($stateProvider) {

  $stateProvider.state('admin', {
    url: '/admin',
    views : {
      "" : {
        templateUrl : 'admin/admin.html'
      },
      "viewHeader@admin" : {
        templateUrl : 'admin/partial/header.html',
        controller : 'AdminHeaderController',
        controllerAs: 'vm'
      },
      "@admin" : {
        templateUrl : 'admin/pages/login.html',
        controller : 'AdminLoginController',
        controllerAs: 'vm'
      },
      "viewFooter@admin" : {
        templateUrl : 'admin/partial/footer.html'
      }
    }
  }).state('recoverpassword', {
    url: '/recoverpassword',
    templateUrl: 'admin/pages/recoverpassword.html',
    controller : 'recoverpasswordController',
    controllerAs: 'vm'
  }).state('admin.profile', {
    url: '/profile',
    templateUrl: 'admin/pages/profile.html',
    controller : 'AdminProfileController',
    controllerAs: 'vm'
  }).state('admin.activateuser', {
    url: '/activateuser/:tokenid',
    templateUrl: 'admin/pages/activateuser.html',
    controller : 'activateuserController',
    controllerAs: 'vm'
  }).state('admin.custommessages', {
    url: '/custommessages/:messageheader/:messagetext',
    templateUrl: 'admin/pages/custommessages.html',
    controller : 'custommessagesController',
    controllerAs: 'vm'
  }).state('admin.changepassword', {
    url: '/changepassword/:tokenid',
    templateUrl: 'admin/pages/changepassword.html',
    controller : 'changepasswordController',
    controllerAs: 'vm'
  }).state('admin.superadmin', {
    url: '/superadmin',
    templateUrl: 'superadmin/superadmin.html'
  });
  /* Add New States Above */

});

