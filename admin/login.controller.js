angular.module('admin').controller('AdminLoginController', function(userService, $state, $cookies){

  var vm = this;
  vm.submitLoginForm = submitForm;

  init();

  function init(){

    if ($cookies.get('AdminSession')){
        $state.go('superadmin');
    }
  }

  function submitForm(){
    if(!vm.username){
      window.alert('Please enter your email address.');
      return;
    }

    if(!vm.password){
      window.alert('Please enter your password.');
      return;
    }

    //submit form
    userService.adminLogin(vm.username, vm.password).then(function(data){
      //$location.path( '/editProfile' );
      //console.log('login controller');
      //console.log(data);
      vm.status = data.data.authToken;
      $state.go('superadmin.offer');
      //$state.go('admin.superadmin');
    }, function(data){
     // alert('dd');
      if(data.data && data.data.message){
        window.alert(data.data.message);
        //toastr.info('Code:' + data.data.code + '. Message: ' + data.data.message);
      }
    });

  }


});
