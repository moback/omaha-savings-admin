/**
 * @ngdoc function
 * @name parent.controller:ParentHeaderController
 * @description
 * # ParentHeaderController
 * Controller of the parent
 */
angular.module('superadmin')
  .controller('superadminHeaderController', function( $rootScope, $location,userService,$state, $cookies) {

    var vm = this;
    vm.logout = logout;
    vm.getUserId = getUserId;
    vm.userId = "Admin";
    getUserId();

    function logout(){
      //submit form
      userService.adminLogout();
      $state.go('admin');
    }

    function getUserId(){
      vm.userId = $cookies.get('superAdminUserId');
      return $cookies.get('superAdminUserId');
    }

  });
