/**
 * Created by user on 3/22/16.
 */
/**
 * Created by user on 3/22/16.
 */
/**
 * Created by user on 3/22/16.
 */
/**
 * Created by user on 3/21/16.
 */
/**
 * Created by user on 11/16/15.
 */
angular.module('superadmin').controller('offerEditCtrl', function (userService,$location,$cookies,$uibModal,apiomahaService,$state,$scope,$http,endpoints,$stateParams) {

  var vm = this;

  vm.allpartnerlist = [];
  vm.submitOfferForm = submitOfferForm;
  vm.cancel = cancelForm;

  vm.offerType = ['regional','local','both'];
  vm.type = vm.offerType[0];
  $scope.mindate = new Date();
  vm.startDateTime = new Date();
  vm.actstartdatePickerIsOpen = false;
  vm.minParticipants = 1;
  vm.maxParticipants = 1;
  vm.slelctedpartner = "";
  vm.offerurl = "";
  vm.offername = "";
  vm.barcodeurl = "";
  vm.barcodename = "";

  vm.barCodeurl="";
  vm.barCodeName="";
  vm.offerImageurl="";
  vm.offerImageName="";
  vm.streamImageurl = "";
  vm.streamImageName = "";
  vm.orgbarCodeurl="";
  vm.orgbarCodeName="";
  vm.orgofferImageurl="";
  vm.orgofferImageName="";
  vm.orgstreamImageurl="";
  vm.orgstreamImageName="";

  vm.startdate = new Date();
  vm.isBarImageRemoved = false;
  vm.checkOfferImage = false;
  vm.checkStreamImage = false;
  vm.showofferimageerror = false;
  vm.showofferImageError = false;
  vm.showStreamImageError = false;
  vm.showBarcodeImageError = false;
  $scope.isReadyToUpdate = true;





  init();


  function init() {

    vm.offerid = $stateParams.offerid;



    apiomahaService.getofferlistwId(vm.offerid).then(function(data){


      if(data.data){

        vm.objectId = data.data.objectId;
        vm.description = data.data.description;
        vm.title = data.data.title;
        //vm.type = data.data.type;
        vm.isFeatured = data.data.isFeatured;
        vm.restrictions = data.data.restrictions;
        vm.minParticipants = data.data.minParticipants;
        vm.maxParticipants = data.data.maxParticipants;
        vm.expirationDateTime = new Date(data.data.expirationDateTime.iso);
        vm.startDateTime = new Date(data.data.startDateTime.iso);
        if (data.data.partnerPoint) {
          vm.slelctedpartner = data.data.partnerPoint.objectId;
        }

        vm.offerImageurl = data.data.teaserPhoto.url;
        vm.offername = data.data.teaserPhoto.name;

        vm.streamImageurl = data.data.streamPhoto.url ? data.data.streamPhoto.url : '';
        vm.streamname = data.data.streamPhoto ? data.data.streamPhoto.name : '';

        vm.barCodeurl = data.data.barcodeImage.url ? data.data.barcodeImage.url : '';
        vm.barcodename = data.data.barcodeImage ? data.data.barcodeImage.name : '';

        console.log("vm.offerImageurl", vm.offerImageurl, "vm.streamImageurl", vm.streamImageurl);

        vm.orgofferImageurl=vm.offerurl;
        vm.orgofferImageName=vm.offername;
        vm.orgstreamImageurl=vm.offerurl;
        vm.orgstreamImageName=vm.offername;
        vm.orgbarCodeurl=vm.barcodeurl;
        vm.orgbarCodeName=vm.barcodename;

      //  vm.startdate =  new Date(data.data.startDateTime.iso);
        if (data.data.type == 'local'){
          vm.type = vm.offerType[1];
        }
        if (data.data.type == 'regional'){
          vm.type = vm.offerType[0];
        }
        if (data.data.type == 'regional,local'){
          vm.type = vm.offerType[2];
        }

        /*
         vm.barcodeurl = response.data.logo.url;
         vm.barcodename = response.data.logo.name;
         */



        apiomahaService.getpartnerlist().then(function(data){

          if(data.data && data.data.results.length >= 0){

            vm.allpartnerdata = _.sortBy(data.data.results, 'name');
            vm.allpartnerlist = _.map(vm.allpartnerdata, function(currentObject) {
              return _.pick(currentObject, "objectId", "name");
            });
            var selectedvalue = _.findWhere(vm.allpartnerlist, { objectId : vm.slelctedpartner});
            vm.availableOptions = vm.allpartnerlist;
            vm.selectedpartner = selectedvalue;


          }
        });







      }
    });

  }


  $scope.$watch(function () {
    return vm.actstartdatePickerIsOpen;
  },function(value){
    vm.opens.push("actstartdatePickerIsOpen: " + value + " at: " + new Date());
  });


  vm.actstartdatePickerOpen = function ($event) {

    if ($event) {
      $event.preventDefault();
      $event.stopPropagation();
    }
    this.actstartdatePickerIsOpen = true;
  };


  //vm.expirationDateTime = new Date(Date.now() + 864e5 * 30);
  vm.expirationDateTime = new Date();
  vm.actenddatePickerIsOpen = false;
  vm.opens = [];

  $scope.$watch(function () {
    return vm.actenddatePickerIsOpen;
  },function(value){
    vm.opens.push("actenddatePickerIsOpen: " + value + " at: " + new Date());
  });

  vm.actenddatePickerOpen = function ($event) {

    if ($event) {
      $event.preventDefault();
      $event.stopPropagation();
    }
    this.actenddatePickerIsOpen = true;
  };


  vm.startdateclick = function ($event) {

    vm.startdateOpenStatus = true;

  // vm.dpOpenStatus[id] = true
    //alert('wwwww');
   //this.actenddatePickerIsOpen = true;

  };





  var toUTCDate = function(date){
    var _utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    return _utc;


  };


  function submitOfferForm(isValid) {
    console.log("offer photo",vm.offerImageurl);
    console.log("offer stream photo",vm.streamImageurl);
    if (isValid && !vm.showofferimageerror && !vm.showStreamImageError && !vm.showBarcodeImageError) {
      console.log("form is amazing", vm.showofferimageerror, vm.showStreamImageError ,vm.showBarcodeImageError);
    }else{

      console.log("form is not valid", isValid,vm.showofferimageerror, vm.showStreamImageError ,vm.showBarcodeImageError);
      return;
    }

    if(vm.expirationDateTime < vm.startDateTime){
      console.log("date range issue");
      //window.alert("Expiration date can't be less than Start date");
      //return;
    }

    if(vm.minParticipants > vm.maxParticipants){
      window.alert("Mininum no. of participants can't be more than Maximum no. participants");
      return;
    }

    var startDate = new Date(
      vm.startDateTime.getFullYear(), vm.startDateTime.getMonth(), vm.startDateTime.getDate(),
      1, 1, 1 //first hour of the day
    );

    var endDate = new Date(
      vm.expirationDateTime.getFullYear(), vm.expirationDateTime.getMonth(), vm.expirationDateTime.getDate(),
      23, 1, 1 //last hour of the day
    );

    
    if( endDate.toISOString() < startDate.toISOString()){
      window.alert("Expiration date can't be less than Start date");
      return;
    }
    

if (isValid && !vm.showofferimageerror && !vm.showStreamImageError && !vm.showBarcodeImageError) {
    var startDateTimeobj = {};
    startDateTimeobj.__type = 'Date';
    startDateTimeobj.iso = startDate.toISOString();

    var expDateTimeobj = {};
    expDateTimeobj.__type = 'Date';
    expDateTimeobj.iso = endDate.toISOString();

    var barcodeImageobj = new Object();
    barcodeImageobj.__type = 'File';

    if (vm.barcodeImage == '')
    {

        barcodeImageobj.url = "";
        barcodeImageobj.name = "";
    }
    else
    {
      if (vm.barCodeurl == "")
      {
        barcodeImageobj.url = vm.orgbarCodeurl;
      }
      else
      {
        barcodeImageobj.url = vm.barCodeurl;
      }

      if (vm.barCodeName == "")
      {
        barcodeImageobj.name = vm.orgbarCodeName;
      }
      else
      {
        barcodeImageobj.name = vm.barCodeName;

      }
    }
    vm.barcodeImage = barcodeImageobj;





    var teaserPhotoobj = new Object();
    teaserPhotoobj.__type = 'File';
    if (vm.offerImageurl == "")
    {
      teaserPhotoobj.url = vm.orgofferImageurl;
    }
    else
    {
      teaserPhotoobj.url = vm.offerImageurl;
    }

    if (vm.offerImageName == "")
    {
      teaserPhotoobj.name = vm.orgofferImageName;
    }
    else
    {
      teaserPhotoobj.name = vm.offerImageName;
    }
    vm.teaserPhoto = teaserPhotoobj;


    var streamPhotoobj = new Object();
    streamPhotoobj.__type = 'File';
    if (vm.streamImageurl == "")
    {
      streamPhotoobj.url = vm.orgstreamImageurl;
    }
    else
    {
      streamPhotoobj.url = vm.streamImageurl;
    }

    if (vm.streamImageName == "")
    {
      streamPhotoobj.name = vm.orgstreamImageName;
    }
    else
    {
      streamPhotoobj.name = vm.streamImageName;
    }
    vm.streamPhoto = streamPhotoobj;


    var partnerobj = new Object();
    partnerobj.__type = 'Pointer';
    partnerobj.className = 'partners';
    if (!vm.selectedpartner) {
      window.alert('Please select a partner before continuing');
      return false;
    }
    partnerobj.objectId = vm.selectedpartner.objectId;
    vm.partnerPoint = partnerobj;

    //,'barcodeImage','teaserPhoto'

    //var createofferdata = _.pick(vm,'type','title','description','restrictions','minParticipants','maxParticipants','isFeatured','startDateTime','expirationDateTime','partnerPoint','barcodeImage','teaserPhoto','streamPhoto');
    var createofferdata = _.pick(vm,'type','title','description','restrictions','minParticipants','maxParticipants','isFeatured','partnerPoint','barcodeImage','teaserPhoto','streamPhoto');
    createofferdata.startDateTime = startDateTimeobj;
    createofferdata.expirationDateTime = expDateTimeobj;
    if (createofferdata.type == 'both'){
      createofferdata.type = 'regional,local';
    }

    //var partnerobj = new Object();
    //partnerobj.partnerPoint = vm.selectedpartner.objectId;
    //var createofferdata  = _.extend(createofferdata,partnerobj);

    //console.log(createofferdata);
    //console.log('submit form');
    //return false;

    apiomahaService.updatemobackdata('offers', vm.objectId, createofferdata).then(function(response){
      console.log(response.data);

      if (response.data.updatedAt){
        window.alert('Offer successfully updated');
      }

      //console.log(response.data);
      //window.alert(response.data.message);

      $state.go('superadmin.offer');
      if(response.data && response.data.id){
        //var id = response.data.id;
        $state.go('superadmin.offer');
      }
    }, function(response){
      if(response.data && response.data.message){
        window.alert(response.data.message.message);
      }
    });
   }
}



  function cancelForm() {
    //$uibModalInstance.dismiss('cancel');
    $state.go('superadmin.offer');
  }

vm.processImage = function(file, type){
  //Offer Image
  if (type == 'OFFER') {
        var offerMaxImageWidth = 1280, offerMaxImageHeight = 1031;
        var imgWidth, imgHeight;
        var _URL = window.URL || window.webkitURL;
        var img = new Image();
        img.onload = function() {
            imgWidth  = this.width;
            imgHeight = this.height;
            if (imgWidth === offerMaxImageWidth && imgHeight === offerMaxImageHeight) {
              console.log("valid dimension");
              $scope.isReadyToUpdate = false;
              vm.showofferimageerror = false;
              uploadOfferFiles(file);
            }else{
              vm.showofferimageerror = true;
              console.log("invalid dimension");
              return;
            }
        };
        img.onerror = function() {
            console.log( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
  }
  //Offer Image
  if (type == 'STREAM') {
        var streamMaxImageWidth = 1280, streamMaxImageHeight = 640;
        var imgWidth, imgHeight;
        var _URL = window.URL || window.webkitURL;
        var img = new Image();
        img.onload = function() {
            imgWidth  = this.width;
            imgHeight = this.height;
            if (imgWidth === streamMaxImageWidth && imgHeight === streamMaxImageHeight) {
              console.log("valid stream image dimension");
              $scope.isReadyToUpdate = false;
              vm.showStreamImageError = false;
              uploadstreamFiles(file);
            }else{
              console.log("invalid stream image dimension");
              vm.showStreamImageError = true;
              return;
            }
        };
        img.onerror = function() {
            console.log( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
  }
  //barcode image
  if (type == 'BARCODE') {
        var barcodeMaxImageWidth = 915, barcodeMaxImageHeight = 225;
        var imgWidth, imgHeight;
        var _URL = window.URL || window.webkitURL;
        var img = new Image();
        img.onload = function() {
            imgWidth  = this.width;
            imgHeight = this.height;
            if (imgWidth === barcodeMaxImageWidth && imgHeight === barcodeMaxImageHeight) {
              console.log("valid barcode image dimension");
              $scope.isReadyToUpdate = false;
              vm.showBarcodeImageError = false;
              uploadBarcodeFiles(file);
            }else{
              console.log("invalid barcode image dimension");
              vm.showBarcodeImageError = true;
              return;
            }
        };
        img.onerror = function() {
            console.log( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
  }

}

  var uploadOfferFiles = function(file) {
    //vm.f = file;
    //vm.errFile = errFiles && errFiles[0];
    if (file) {

      if(vm.teaserPhoto){
        console.log("check teaserPhoto values");
        vm.checkOfferImage = false;
      }
      
      //alert('offer');

      var fd = new FormData();
      fd.append('file', file);
      $http.put(endpoints.MobackFileManager, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined,'X-Moback-Application-Key': endpoints.MobackApplicationKey}
        })
        .success(function(data){
          //console.log(data);

          if (data.code == '1000')
          {
            $scope.isReadyToUpdate = true;
            vm.offerImageurl = data.url;
            vm.offerImageName = data.name;

          }

        })
        .error(function(data){
          //alert('error'+data);
          console.log('error'+data);
        });




    }
  };


  var uploadstreamFiles = function(file) {

    //vm.f = file;
    //vm.errFile = errFiles && errFiles[0];
    if (file) {

      if(vm.streamPhoto){
        console.log("check streamPhoto values");
        vm.checkStreamImage = false;
      }

      //alert('offer');

      var fd = new FormData();
      fd.append('file', file);
      $http.put(endpoints.MobackFileManager, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined,'X-Moback-Application-Key': endpoints.MobackApplicationKey}
        })
        .success(function(data){
          //console.log(data);

          if (data.code == '1000') {
            $scope.isReadyToUpdate = true;
            vm.streamImageurl = data.url;
            vm.streamImageName = data.name;
          }
        })
        .error(function(data){
          //alert('error'+data);
          console.log('error'+data);
        });




    }
  };



  var uploadBarcodeFiles = function(file) {
//alert('sssa');

    vm.f = file;

    //vm.errFile = errFiles && errFiles[0];
    if (file) {

      // alert('barcode');

      var fd = new FormData();
      fd.append('file', file);
      $http.put(endpoints.MobackFileManager, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined,'X-Moback-Application-Key': endpoints.MobackApplicationKey}
        })
        .success(function(data){

          if (data.code == '1000')
          {
            $scope.isReadyToUpdate = true;
            //alert(data.url);
            vm.barCodeurl = data.url;
            vm.barCodeName = data.name;
          }

        })
        .error(function(data){
          //alert('error'+data);
          console.log('error'+data);
        });

    }
    else
    {


    }




  };















});
