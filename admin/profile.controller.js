angular.module('admin').controller('AdminProfileController', function(adminService, $cookies, $uibModal, $stateParams, $q,$state,userService,apiomahaService){

  var vm = this;
  vm.updateProfileForm = updateProfile;
  vm.cancel = cancelForm;
  vm.userid = "";
  vm.email ="";
  vm.objectId="";
  vm.password="";
  vm.confirmpassword=""
  vm.fName ="";
  vm.lName ="";

  init();

  function init(){
    getProfile();
  }

  function getProfile(){


    var adminUserName = $cookies.get('superAdminUserId');
    //alert(adminUserName);
    userService.getUserByUserId(adminUserName).then(function(data){

      if (data.data.results[0])
      {
        vm.userid = data.data.results[0].userId;
        vm.email = data.data.results[0].email;
        vm.objectId= data.data.results[0].objectId;
        vm.fName = data.data.results[0].fName;
        vm.lName=data.data.results[0].lName;
      }

    });
  }


  function cancelForm() {

    //$uibModalInstance.dismiss('cancel');
    $state.go('superadmin.announcement');
  }


  function updateProfile(){

    console.log('1a11111111');
    console.log(vm);
    console.log('2a222222222');

    if (vm.password=="")
    {
      var updateuserdata = _.pick(vm,'fName','lName','email','city','zip','phone');

    }
    else {

      var updateuserdata = _.pick(vm,'fName','lName','email','city','zip','phone','password');

    }






    apiomahaService.updatemobackdata('__appUsers',vm.objectId,updateuserdata).then(function(response){

      window.alert('User successfully updated');
      //console.log(response.data);


      //if (response.data.success){
      //  window.alert('Partner successfully updated');
      //}
      //console.log(response.data);
      //window.alert(response.data.message);

      $state.go('superadmin.announcement');
      if(response.data && response.data.id){
        //var id = response.data.id;
        $state.go('superadmin.announcement');
      }
    }, function(response){
      if(response.data && response.data.message){
        window.alert(response.data.message.message);
      }
    });





    /*

    userService.getUser().then(function(data){

      console.log('1a11111111');
      console.log(data.data.user.objectId);
      console.log('2a222222222');
    }, function(data){
      if(data.data && data.data.message){
        window.alert(data.data.message);
        //toastr.info('Code:' + data.data.code + '. Message: ' + data.data.message);
      }

    });




    var adminUserName = $cookies.get('superAdminUserId');
    var formData = {
      "schoolName": vm.schoolName
    };

    adminService.updateAdminProfile(adminUserName, formData).then(function(data){
      console.log(data.data);
      if(data.data && data.data.message){
        window.alert(data.data.message);
        getProfile();
      }
    }, function(data){
      if(data.data && data.data.message){
        window.alert(data.data.message.propertyName + ': ' + data.data.message.message);
      }
    });
    */





  }

/*

  var compareTo = function() {
    return {
      require: "ngModel",
      scope: {
        otherModelValue: "=compareTo"
      },
      link: function(scope, element, attributes, ngModel) {

        ngModel.$validators.compareTo = function(modelValue) {
          return modelValue == scope.otherModelValue;
        };

        scope.$watch("otherModelValue", function() {
          ngModel.$validate();
        });
      }
    };
  };

  */

}).directive('wjValidationError', function () {
  return {
    require: 'ngModel',
    link: function (scope, elm, attrs, ctl) {
      scope.$watch(attrs['wjValidationError'], function (errorMsg) {
        elm[0].setCustomValidity(errorMsg);
        ctl.$setValidity('wjValidationError', errorMsg ? false : true);
      });
    }
  };
});


