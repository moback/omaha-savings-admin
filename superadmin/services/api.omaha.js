/**
 * Created by user on 3/21/16.
 */
/**
 * Created by user on 11/16/15.
 */
angular.module('superadmin')
  .service('apiomahaService', function (endpoints, $cookies, $q, apiService,$http) {


    this.sayhello = function () {

      return "hello";
    }



    this.getpartnerlist = function(){
      return $q(function (resolve, reject) {

        //var apiHeaders = {
          //'Content-Type' : 'application/json'
          //'AuthToken' : $cookies.get('superAdminSession')
          //'AuthToken' : 'bmFuaGFpc3VwZXJ8MTQ0Njg0MTMwMzk0NnxbU1VQRVJBRE1JTl18NTYzOWE3ZDI4ZDlmMmQ1MjUyMmEyMzQ0LjlhYzQxM2VlNTFjN2JhZTE2ZDlmMWQzNDY5OTRkZWUwMDY2MjcyYTY2MTc0YjZkZGJhNGY2MTVkZTlhYzk3M2Q='
        //};

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'GET',
          url: endpoints.Mobackcollection + 'partners',
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };


    this.createpartner = function(postData){
      return $q(function (resolve, reject) {

        //var apiHeaders = {
        //'Content-Type' : 'application/json'
        //'AuthToken' : $cookies.get('superAdminSession')
        //'AuthToken' : 'bmFuaGFpc3VwZXJ8MTQ0Njg0MTMwMzk0NnxbU1VQRVJBRE1JTl18NTYzOWE3ZDI4ZDlmMmQ1MjUyMmEyMzQ0LjlhYzQxM2VlNTFjN2JhZTE2ZDlmMWQzNDY5OTRkZWUwMDY2MjcyYTY2MTc0YjZkZGJhNGY2MTVkZTlhYzk3M2Q='
        //};

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'POST',
          url: endpoints.Mobackcollection + 'partners',
          data:postData,
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });

      });
    };


    this.deletepartner = function(id){
      return $q(function (resolve, reject) {

        //var apiHeaders = {
        //'Content-Type' : 'application/json'
        //'AuthToken' : $cookies.get('superAdminSession')
        //'AuthToken' : 'bmFuaGFpc3VwZXJ8MTQ0Njg0MTMwMzk0NnxbU1VQRVJBRE1JTl18NTYzOWE3ZDI4ZDlmMmQ1MjUyMmEyMzQ0LjlhYzQxM2VlNTFjN2JhZTE2ZDlmMWQzNDY5OTRkZWUwMDY2MjcyYTY2MTc0YjZkZGJhNGY2MTVkZTlhYzk3M2Q='
        //};

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'DELETE',
          url: endpoints.Mobackcollection + 'partners/'+id,
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };

    this.deleteoffer = function(id){
      return $q(function (resolve, reject) {

        //var apiHeaders = {
        //'Content-Type' : 'application/json'
        //'AuthToken' : $cookies.get('superAdminSession')
        //'AuthToken' : 'bmFuaGFpc3VwZXJ8MTQ0Njg0MTMwMzk0NnxbU1VQRVJBRE1JTl18NTYzOWE3ZDI4ZDlmMmQ1MjUyMmEyMzQ0LjlhYzQxM2VlNTFjN2JhZTE2ZDlmMWQzNDY5OTRkZWUwMDY2MjcyYTY2MTc0YjZkZGJhNGY2MTVkZTlhYzk3M2Q='
        //};

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'DELETE',
          url: endpoints.Mobackcollection + 'offers/'+id,
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };


    this.getpartner = function(id){
      return $q(function (resolve, reject) {

        //var apiHeaders = {
        //'Content-Type' : 'application/json'
        //'AuthToken' : $cookies.get('superAdminSession')
        //'AuthToken' : 'bmFuaGFpc3VwZXJ8MTQ0Njg0MTMwMzk0NnxbU1VQRVJBRE1JTl18NTYzOWE3ZDI4ZDlmMmQ1MjUyMmEyMzQ0LjlhYzQxM2VlNTFjN2JhZTE2ZDlmMWQzNDY5OTRkZWUwMDY2MjcyYTY2MTc0YjZkZGJhNGY2MTVkZTlhYzk3M2Q='
        //};

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'GET',
          url: endpoints.Mobackcollection + 'partners/'+id,
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };


    this.updatepartner = function(partnerId,postData){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'PUT',
          url: endpoints.Mobackcollection + 'partners/' + partnerId,
          data:postData,
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };


    /*
     this.getbooklist = function(){
     var req = {
     method: 'GET',
     url: endpoints.super + 'superadmin/books'
     };

     return apiService.adminAPICall(req);
     };


     });
     */



    this.getofferlist = function(){
      return $q(function (resolve, reject) {

        //var apiHeaders = {
        //'Content-Type' : 'application/json'
        //'AuthToken' : $cookies.get('superAdminSession')
        //'AuthToken' : 'bmFuaGFpc3VwZXJ8MTQ0Njg0MTMwMzk0NnxbU1VQRVJBRE1JTl18NTYzOWE3ZDI4ZDlmMmQ1MjUyMmEyMzQ0LjlhYzQxM2VlNTFjN2JhZTE2ZDlmMWQzNDY5OTRkZWUwMDY2MjcyYTY2MTc0YjZkZGJhNGY2MTVkZTlhYzk3M2Q='
        //};

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'GET',
          url: endpoints.Mobackcollection + 'offers?order=-expirationDateTime&include=partnerPoint',
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };

    this.getofferlistwId = function(objectId){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'GET',
          url: endpoints.Mobackcollection + 'offers/' + objectId + '?include=partnerPoint',
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };

    this.updateoffer = function(Id,postData){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'PUT',
          url: endpoints.Mobackcollection + 'offers/' + Id,
          data:postData,
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };


    this.getmobackdata = function(tableName){
      return $q(function (resolve, reject) {

        //var apiHeaders = {
        //'Content-Type' : 'application/json'
        //'AuthToken' : $cookies.get('superAdminSession')
        //'AuthToken' : 'bmFuaGFpc3VwZXJ8MTQ0Njg0MTMwMzk0NnxbU1VQRVJBRE1JTl18NTYzOWE3ZDI4ZDlmMmQ1MjUyMmEyMzQ0LjlhYzQxM2VlNTFjN2JhZTE2ZDlmMWQzNDY5OTRkZWUwMDY2MjcyYTY2MTc0YjZkZGJhNGY2MTVkZTlhYzk3M2Q='
        //};

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'GET',
          url: endpoints.Mobackcollection + tableName,
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };

    this.getmobackdatawId = function(tableName,objectId){
      return $q(function (resolve, reject) {

        //var apiHeaders = {
        //'Content-Type' : 'application/json'
        //'AuthToken' : $cookies.get('superAdminSession')
        //'AuthToken' : 'bmFuaGFpc3VwZXJ8MTQ0Njg0MTMwMzk0NnxbU1VQRVJBRE1JTl18NTYzOWE3ZDI4ZDlmMmQ1MjUyMmEyMzQ0LjlhYzQxM2VlNTFjN2JhZTE2ZDlmMWQzNDY5OTRkZWUwMDY2MjcyYTY2MTc0YjZkZGJhNGY2MTVkZTlhYzk3M2Q='
        //};

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'GET',
          url: endpoints.Mobackcollection + tableName+ '/' + objectId,
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };

    this.updatemobackdata = function(tableName,Id,postData){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'PUT',
          url: endpoints.Mobackcollection + tableName + '/' + Id,
          data:postData,
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };


    this.createmobackdata = function(tableName,postData){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'POST',
          url: endpoints.Mobackcollection + tableName,
          data:postData,
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          //console.log('inside service');
          if(data.data){
            console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };



    this.getgeolocation = function(address){
      return $http.get(endpoints.googleaddressapi + address)
        .success(function(mapData){
        return mapData// what you return here will be the results of then
      });
    }



    this.getRedeemedOffers = function(objectId){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'GET',
          url: endpoints.Mobackcollection + 'redeemedOffers?limit=1000&include=partner,offer,user',
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };



    this.checkDupPartner = function(partnername){

      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'GET',
          url:  endpoints.Mobackcollection + 'partners?where={\"name\":\"' + encodeURIComponent(partnername)  +'\"}',
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            //console.log('found partner');
            //console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };


    this.recordCount = function(){

      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'GET',
          url:  endpoints.Mobackcollection + '__count',
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            //console.log('found partner');
            //console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };





    this.gettotalrecords = function(tablename){

      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };


        var req = {
          method: 'GET',
          url:  endpoints.Mobackcollection + tablename +'?op=count&where1==1',
          headers: apiHeaders
        };

        //console.log('2222222222');
       // console.log(req);

        apiService.apiCall(req).then(function(data){
          if(data.data){
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };



    this.getusersdatacsv = function(rskip,rlimit){

      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        //var condi="";
        //condi += "";

        //var userobj = new Object();
        //userobj.objectId = userid;
        //userobj.__type = 'Pointer';
        //userobj.className = "__appUsers";

        //var usercondiobj = new Object();
        //usercondiobj.User = userobj;

        // var boardobj = new Object();
        // boardobj.Board_Id = boardid;


        // var condiobj = new Object();
        //  condiobj.$and = [usercondiobj,boardobj];



        var req = {
          method: 'GET',
          url:  endpoints.Mobackcollection + '__appUsers'+'?skip='+rskip+'&limit='+rlimit,
          headers: apiHeaders
        };

        //console.log('2222222222');
        //console.log(req);

        apiService.apiCall(req).then(function(data){
          if(data.data){
            //console.log('found users '+rskip);
            //console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };


    this.getofferredDatacsv = function(rskip,rlimit){

      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        //var condi="";
        //condi += "";

        //var userobj = new Object();
        //userobj.objectId = userid;
        //userobj.__type = 'Pointer';
        //userobj.className = "__appUsers";

        //var usercondiobj = new Object();
        //usercondiobj.User = userobj;

        // var boardobj = new Object();
        // boardobj.Board_Id = boardid;


        // var condiobj = new Object();
        //  condiobj.$and = [usercondiobj,boardobj];



        var req = {
          method: 'GET',
          url:  endpoints.Mobackcollection + 'redeemedOffers'+'?skip='+rskip+'&limit='+rlimit+'&include=partner,offer,user',
          //url:  endpoints.Mobackcollection + 'redeemedOffers'+'?skip='+rskip+'&limit='+rlimit,
          headers: apiHeaders
        };

       // console.log('2222222222');
       // console.log(req);

        apiService.apiCall(req).then(function(data){
          if(data.data){
            //console.log('found users '+rskip);
            //console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };






    this.getOffersInDateRangeCount = function(fromdate,todate){
      return $q(function (resolve, reject) {


        var searchcondi = {'$and':[{'createdAt':{'$gt':{'__type':'Date','iso':fromdate}}},{'createdAt':{'$lt':{'__type':'Date','iso':todate}}}]}

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'GET',
          url: endpoints.Mobackcollection + 'redeemedOffers?op=count&where=' + encodeURI(JSON.stringify(searchcondi)),
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };



    this.getOffersInDateRange = function(fromdate,todate,rskip,limit){

      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var searchcondi = {'$and':[{'createdAt':{'$gt':{'__type':'Date','iso':fromdate}}},{'createdAt':{'$lt':{'__type':'Date','iso':todate}}}]}


        var req = {
          method: 'GET',
          url:  endpoints.Mobackcollection + 'redeemedOffers'+'?skip='+rskip+'&limit='+limit+'&include=partner,offer,user&where=' + encodeURI(JSON.stringify(searchcondi)),
          headers: apiHeaders
        };


        apiService.apiCall(req).then(function(data){
          if(data.data){
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };


    this.getOffersInDateRangeSort = function(fromdate,todate,rskip,limit,orderby){

      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var searchcondi = {'$and':[{'createdAt':{'$gt':{'__type':'Date','iso':fromdate}}},{'createdAt':{'$lt':{'__type':'Date','iso':todate}}}]}


        var req = {
          method: 'GET',
          url:  endpoints.Mobackcollection + 'redeemedOffers'+'?skip='+rskip+'&order='+ orderby + '&limit='+limit+'&include=partner,offer,user&where=' + encodeURI(JSON.stringify(searchcondi)),
          headers: apiHeaders
        };


        apiService.apiCall(req).then(function(data){
          if(data.data){
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };



    this.getpartneremaillist = function(){
      return $q(function (resolve, reject) {

        //var apiHeaders = {
        //'Content-Type' : 'application/json'
        //'AuthToken' : $cookies.get('superAdminSession')
        //'AuthToken' : 'bmFuaGFpc3VwZXJ8MTQ0Njg0MTMwMzk0NnxbU1VQRVJBRE1JTl18NTYzOWE3ZDI4ZDlmMmQ1MjUyMmEyMzQ0LjlhYzQxM2VlNTFjN2JhZTE2ZDlmMWQzNDY5OTRkZWUwMDY2MjcyYTY2MTc0YjZkZGJhNGY2MTVkZTlhYzk3M2Q='
        //};

        var searchcondi = {'admin':{'$exists':true}};

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'GET',
          url: endpoints.Mobackcollection + '__appUsers?where='+ encodeURI(JSON.stringify(searchcondi)),
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };






  });
