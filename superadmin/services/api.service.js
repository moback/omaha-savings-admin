/**
 * Created by bhavesh on 11/4/2015.
 */
/**
 * @ngdoc service
 * @name fortesuperadminApp.apiService
 * @description
 * # apiService
 * Service in the fortesuperadminApp.
 */
angular.module('superadmin')
  .service('apiService', function ($http, $cookies, $q, endpoints, $location) {

    var self = this;

    this.apiCall = function(req){
     // alert('wer');
      // this will execute first
      var adminSession = $cookies.get('superAdminSession');
      if (!adminSession){
        //console.log('admin session expires');
        $location.path("/login");
      }
      return $http(req);
      //req.headers = req.headers || {};
     // req.headers.AuthToken = adminSession;


    };

    this.adminAPICall = function(req){

      //add admin session to request
      var adminSession = $cookies.get('superAdminSession');
      req.headers = req.headers || {};
      req.headers.AuthToken = adminSession;
      //req.headers.AuthToken = "bmFuaGFpc3VwZXJ8MTQ0OTg3NzEzNjU1N3xbU1VQRVJBRE1JTl18NTYzOWE3ZDI4ZDlmMmQ1MjUyMmEyMzQ0LmQxOTBiZjg0YWZhMWM1NmFlMzM1MjllM2E4Y2ZmNmVjNDU2ZDdiYThhNzVmZTQyNWQyMzJhNTIzZDM1NjY1M2Q=";
      return $http(req);
    };



    /*make an api get call to a forte service*/
    this.apiCallGet = function(path, header){
      return $q(function (resolve, reject) {
        var forteSession = $cookies.get('superAdminSession');
        header = header || {};
        header['Content-Type'] = 'application/json';
        header['AuthToken'] = $cookies.get('superAdminSession');
       console.log(header['AuthToken']);
       console.log(header);
        var req = {
          method: 'GET',
          url: endpoints.main + path,
          headers: header
        };

        self.apiCall(req).success(function(data){
         console.log('success');
          console.log(data);
          resolve(data);
        }).
          error(function(data) {
             console.log(data);
            //session is invalid and user has been logged out
            if(data.code && data.code == '118'){
              $location.path("/logout");
            }
            if(data.code && data.code == '119'){
              $location.path("/logout");
            }
            reject(data);
          });
      });
    };

    this.apiCallPost = function(path, toPost){
      return $q(function (resolve, reject) {
        var forteSession = $cookies.get('superAdminSession');
        var apiHeaders = {
          'Content-Type' : 'application/json',
          'sessionid' : forteSession
        };

        var req = {
          method: 'POST',
          url: endpoints.main + path,
          headers: apiHeaders,
          data: toPost
        };

        self.apiCall(req).success(function(data){
          resolve(data);
        }).
          error(function(data) {
            //session is invalid and user has been logged out
            if(data.code && data.code == '118'){
              $location.path("/logout");
            }
            if(data.code && data.code == '119'){
              $location.path("/logout");
            }
            reject(data);
          });
      });
    };

    this.apiCallPostHeader = function(path, customHeader){
      return $q(function (resolve, reject) {
        customHeader['Content-Type'] = 'application/json';
        customHeader['sessionid'] = $cookies.get('superAdminSession');

        var req = {
          method: 'POST',
          url: endpoints.main + path,
          headers: customHeader
        };

        self.apiCall(req).success(function(data){
          resolve(data);
        }).
          error(function(data) {
            //session is invalid and user has been logged out
            if(data.code && data.code == '118'){
              $location.path("/logout");
            }
            if(data.code && data.code == '119'){
              $location.path("/logout");
            }
            reject(data);
          });
      });
    };









  });
