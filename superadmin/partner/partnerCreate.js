/**
 * Created by user on 3/21/16.
 */
/**
 * Created by user on 11/16/15.
 */
angular.module('superadmin').controller('partnerCreateCtrl', function (userService,$location,$cookies,$uibModal,apiomahaService,$state, $scope, Upload, $http,endpoints) {

  var vm = this;
  vm.submitLoginForm = submitForm;
  vm.cancel = cancelForm;
  vm.detectionRadius = 1;
  vm.partnerlogourl = "";
  vm.partnerlogname = "";
  vm.partnermapurl = "";
  vm.partnermapname = "";
  vm.open = popupdialog;
  vm.validlogo = false;
  vm.showlogoloading = false;

  vm.validmap = false;
  vm.showmaploading = false;
  vm.emails = "";
  vm.showoLogoImageerror = false;
  vm.showMapImageError = false;
  $scope.isReadyToUpdate = true;

  //vm.logo ="";
  init();

  function init(){
   //vm.open();
    //console.log(ModalInstanceCtrl);
   // ModalInstanceCtrl.cancel();

  }

  /*
  vm.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
  */

  function popupdialog(size) {

    var modalInstance = $uibModal.open({
      animation: vm.animationsEnabled,
      templateUrl: 'myModalContent.html',
      controller: 'ModalInstanceCtrl',
      size: size,
      resolve: {
        items: function () {
          return vm.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      vm.selected = selectedItem;

    }, function () {
     // $log.info('Modal dismissed at: ' + new Date());
    });


  };

/*
  var ModalInstanceCtrl = function ($scope, $uibModalInstance, items) {

    console.log($modalInstance);
    $scope.ok = function () {
      $uibModalInstance.close($scope.selected.item);
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

  }
*/


      /*
  function ModalInstanceController($scope, $uibModalInstance, syncData, asyncData) {

    $scope.asyncData = asyncData;
    $scope.moreData = syncData;

    $scope.save = function() {
      $uibModalInstance.close($scope.theThingIWantToSave);
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    };
  }

*/


  function submitForm(isValid) {
    
    if(vm.detectionRadius % 1 !=0){
      console.log("came inside to check detection value");
      window.alert("Please enter a valid integer value for Detection Radius");
      return;
    }
    //console.log("vm.detectionRadius", vm.detectionRadius);
    if(vm.name == undefined && vm.address1 == undefined && vm.city == undefined && vm.zip == undefined && vm.logo == "" && vm.mapImage == ""){
      console.log("came inside bcz fields are undefined");
      return;
    }

    if (isValid && !vm.showoLogoImageerror && !vm.showMapImageError)
    {


      var logoobj = new Object();
      logoobj.__type = 'File';
      logoobj.name = vm.partnerlogname;
      logoobj.url = vm.partnerlogourl;
      vm.logo = logoobj;


      var mapImageoobj = new Object();
      mapImageoobj.__type = 'File';
      mapImageoobj.name =  vm.partnermapname;
      mapImageoobj.url = vm.partnermapurl;
      vm.mapImage = mapImageoobj;


      $http.get(endpoints.googleaddressapi + vm.address1+ ','+vm.city + ','+'NE' +','+ vm.zip).success(function(mapData) {
        console.log('location1');
        console.log(mapData);
        if (mapData.results.length > 0){
          var locationobj = new Object();
          locationobj.__type = 'GeoPoint';
          locationobj.lat = mapData.results[0].geometry.location.lat;
          locationobj.lon = mapData.results[0].geometry.location.lng;
          vm.location = locationobj;

        }

        var createpartnerdata = _.pick(vm,'name','address1','address2','city','zip','phone','emails','detectionRadius','mapImage','logo','location');

        apiomahaService.createpartner(createpartnerdata).then(function(response){

          if (response.data.objectId && vm.emails !=''){

            userService.assignPartnerId(response.data.objectId,vm.emails).then(function(response){

              console.log('assigned email id');

            }, function(response){
              if(response.data && response.data.message){
                console.log(response.data.message.message);
              }
            });


          }

          if (response.data.success){
            window.alert('Partner successfully created');
          }
          //window.alert(response.data.message);

          $state.go('superadmin.partner');
          if(response.data && response.data.id){
            //var id = response.data.id;
            $state.go('superadmin.partner');
          }
        }, function(response){
          if(response.data && response.data.message){



            window.alert(response.data.message.message);
          }
        });

      });



    }
    else {
      vm.showlogoerror = true;
     // create_main_form.logo.$error.required = true;
     console.log('Invalid form...something went wrong..');
    }


  }


  function cancelForm() {
    //$uibModalInstance.dismiss('cancel');
    $state.go('superadmin.partner');
  };


vm.processImage = function(file, type){
  //logo Image
  if (type == 'LOGO') {
        var logoMaxImageWidth = 1066, logoMaxImageHeight = 497;
        var imgWidth, imgHeight;
        var _URL = window.URL || window.webkitURL;
        var img = new Image();
        img.onload = function() {
            imgWidth  = this.width;
            imgHeight = this.height;
            if (imgWidth === logoMaxImageWidth && imgHeight === logoMaxImageHeight) {
              console.log("valid dimension");
              vm.showoLogoImageerror = false;
              $scope.isReadyToUpdate = false;
              uploadFiles(file);
            }else{
              vm.showoLogoImageerror = true;
              console.log("invalid dimension");
              return;
            }
        };
        img.onerror = function() {
            console.log( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
  }
  //map Image
  if (type == 'MAP') {
        var mapMaxImageWidth = 1280, mapMaxImageHeight = 620;
        var imgWidth, imgHeight;
        var _URL = window.URL || window.webkitURL;
        var img = new Image();
        img.onload = function() {
            imgWidth  = this.width;
            imgHeight = this.height;
            if (imgWidth === mapMaxImageWidth && imgHeight === mapMaxImageHeight) {
              console.log("valid stream image dimension");
              $scope.isReadyToUpdate = false;
              vm.showMapImageError = false;
              uploadFilesmap(file);
            }else{
              console.log("invalid stream image dimension");
              vm.showMapImageError = true;
              return;
            }
        };
        img.onerror = function() {
            console.log( "not a valid file: " + file.type);
        };
        img.src = _URL.createObjectURL(file);
  }
  

}
  var uploadFiles = function(file) {



    if(vm.validlogo)
    {
        //vm.open();
      vm.showlogoloading = true;
        vm.f = file;
        //vm.errFile = errFiles && errFiles[0];
        if (file) {

            var fd = new FormData();
            fd.append('file', file);
            $http.put(endpoints.MobackFileManager, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined,'X-Moback-Application-Key': endpoints.MobackApplicationKey}
              })
              .success(function(data){
                //console.log(data);

                if (data.code == '1000')
                {
                  vm.showoLogoImageerror = false;
                  $scope.isReadyToUpdate = true;

                  //alert(data.url);
                  vm.partnerlogourl = data.url;
                  vm.partnerlogname = data.name;
                  vm.logo = data.url;
                  vm.showlogoloading = false;
                }

              })
              .error(function(data){
                //alert('error'+data);
                console.log('error'+data);
              });

        }

        //vm.modalInstance.dismiss('cancel');
       // vm.cancel();

    }
  }


  var uploadFilesmap = function(file) {


    if(vm.validmap) {
      vm.showmaploading = true;
      vm.f = file;
      //vm.errFile = errFiles && errFiles[0];
      if (file) {

        var fd = new FormData();
        fd.append('file', file);
        $http.put(endpoints.MobackFileManager, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined, 'X-Moback-Application-Key': endpoints.MobackApplicationKey}
          })
          .success(function (data) {
            //console.log(data);

            if (data.code == '1000') {
              //alert(data.url);
              $scope.isReadyToUpdate = true;
              vm.partnermapurl = data.url;
              vm.partnermapname = data.name;
              vm.mapImage = data.url;
              vm.showmaploading = false;
              vm.showMapImageError = false;
            }

          })
          .error(function (data) {
            //alert('error'+data);
            console.log('error' + data);
          });

      }

    }

  }





}).controller('ModalInstanceCtrl', [ '$scope', '$uibModalInstance', 'items', function ($scope, $modalInstance, items) {

  $scope.ok = function () {
    $modalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };


}]).directive('validateUsername', function ($q, apiomahaService) {

  //alert(userService);
  //alert($q);
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, ctrl) {

      /*
      ctrl.$validators.invalidpartner = function(modelValue) {

        if (modelValue){
          //alert(modelValue);
          return true;
        }
      }
      */

      ctrl.$asyncValidators.invalidpartner = function (modelValue, viewValue) {

        return $q(function (resolve, reject) {
          //alert(viewValue);
          //resolve();

          apiomahaService.checkDupPartner(modelValue).then(function (response) {

            if (response.data.results.length == 0)
            {
              resolve();
            }
            else
            {
              reject();
            }

            //console.log('found partner');
            ///console.log(response.data.results.length);

          }, function () {
            reject();
          });
        });
      };



    }
  };
});
