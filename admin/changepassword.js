angular.module('admin').controller('changepasswordController', function(adminService, $cookies, $uibModal, $stateParams, $q,$state,userService){

  var vm = this;
  vm.changepasswordForm = changepassword;
  vm.password="";
  vm.confirmpassword="";
  vm.actoken="";

  init();

  function init(){

    vm.actoken = $stateParams.tokenid;

  }


  function changepassword(){


    userService.changeUserPassword(vm.actoken, vm.password).then(function(data){
      //$location.path( '/editProfile' );

      vm.message = data.data.message;
      // console.log(data);
       if (data.data.code == 1000)
       {
       //vm.message = 'Your password has been successfully changed';
        // $window.alert('Your password has been successfully changed');
         //$window.close();

         //var messagetext = 'Your password has successfully been reset.Log in to enjoy admission discounts and <br/> special savings for Omaha restaurents,shopping,museum and top attractions.';
         var messagetext = 'Your password has successfully been reset.';

         var messageheader = 'CONGRATS!';
         $state.go('admin.custommessages',{ "messageheader": messageheader , "messagetext":messagetext});

       }
       else
       {
         var messagetext = data.data.message+" please resubmit your request.";
         var messageheader = 'ERROR!';
         $state.go('admin.custommessages',{ "messageheader": messageheader , "messagetext":messagetext});
          //vm.message = data.data.message;
       }



    }, function(data){
      // alert('dd');
      if(data.data && data.data.message){
        //window.alert(data.data.message);
      }
    });







  }

}).directive('wjValidationError', function () {
  return {
    require: 'ngModel',
    link: function (scope, elm, attrs, ctl) {
      scope.$watch(attrs['wjValidationError'], function (errorMsg) {
        elm[0].setCustomValidity(errorMsg);
        ctl.$setValidity('wjValidationError', errorMsg ? false : true);
      });
    }
  };
});



