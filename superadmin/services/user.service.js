/**
 * Created by bhavesh on 11/4/2015.
 */
/**
 * @ngdoc service
 * @name fortesuperadminApp.userService
 * @description
 * # userService
 * Service in the fortesuperadminApp.
 */
angular.module('superadmin')
  .service('userService', function (endpoints, $cookies, $q, apiService,apiomahaService) {



    this.adminLogin = function(userid, password){


      return $q(function (resolve, reject) {
        var toPost = {
          // omaha
          //"password": password,
         // "userName": userid
          "password": password,
          "userId": userid
        };

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };


        var req = {
          method: 'POST',
          headers: apiHeaders,
          //omaha
          //url: endpoints.user + 'login',
          url: endpoints.Mobackuser + 'login',
          data: toPost
        };





        apiService.apiCall(req).then(function(data){

          // omaha
          //if(data.data && data.data.ssotoken){
          if (data.data.response.code != '1000'){
            alert(data.data.response.message);
          }
          else
          {

            //console.log('login response');
            //console.log(data.data);

            var rolereq = {
              method: 'GET',
              url: endpoints.Mobackcollection + '__appUsers/' + data.data.response.objectId + '?include=__roles',
              headers: apiHeaders
            };

            apiService.apiCall(rolereq).then(function(rdata){

              if(rdata.data){
                //console.log('login2 response');
                //console.log(rdata.data.__roles);
                if (rdata.data.__roles)
                {

                  var filteredGoal = _.where(rdata.data.__roles.value, {roleName: "admin"});
                  if (filteredGoal.length > 0){


                    if(data.data && data.data.ssotoken){

                      $cookies.put('superAdminSession', data.data.ssotoken);
                      //$cookies.put('superAdminSession', data.data.authToken);
                      $cookies.put('superAdminUserId', userid);
                      // alert('login ' + data.data.ssotoken );
                      // alert('login ' + userid );
                      resolve(data);
                    }


                  }
                  else
                  {
                    alert('You are not authorised ');
                    $cookies.remove('superAdminSession');
                    $cookies.remove('superAdminUserId');
                  }

                }
                else
                {
                  alert('You are not authorised ');
                  $cookies.remove('superAdminSession');
                  $cookies.remove('superAdminUserId');
                }
                resolve(data);
              }


            });


          } //end 1000 code else



          /*
          if(data.data && data.data.ssotoken){
            $cookies.put('superAdminSession', data.data.ssotoken);
            //$cookies.put('superAdminSession', data.data.authToken);
            $cookies.put('superAdminUserId', userid);
           // alert('login ' + data.data.ssotoken );
           // alert('login ' + userid );
            resolve(data);
          }
          */



        }, function(data){ reject(data); });




      });
    };

    this.adminLogout = function(){
      $cookies.remove('superAdminSession');
      $cookies.remove('superAdminUserId');
    };

    this.saveSession = function(forteSession){
      $cookies.put('superAdminSession', forteSession);
    };

    this.getUserPrefences = function(){
      return apiService.apiCallGet('pref/getUserPreferences');
    };

    this.setUserPrefences = function(toSend){
      return apiService.apiCallPost('pref/setUserPreferences', toSend);
    };

    this.isLoggedIn = function(){
      if($cookies.get('superAdminSession')){
        return true;
      }
      return false;
    };


    this.recoverPassword = function(postData){

      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'POST',
          url: endpoints.Mobackuser + 'password/reset',
          data:postData,
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };


    this.getUserDetails = function(userObjectId){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
        };

        var req = {
          method: 'GET',
          url: endpoints.Mobackcollection + '__appUsers/' + userObjectId + '?include=__roles',
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };



    this.getUser = function(){

      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession'),

        };

        var req = {
          method: 'GET',
          url:  endpoints.Mobackuser + 'user',
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };

    this.getUserByUserId = function(userId){
      console.log(userId);
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession'),
        };

        var req = {
          method: 'GET',
         // url:  endpoints.Mobackcollection + '__appUsers?where=%7B\"userId\"%3A\"johnvogel47%40gmail.com\"%7D',
          url:  endpoints.Mobackcollection + '__appUsers?where={\"userId\":\"' + encodeURIComponent(userId)  +'\"}',
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };



    this.getAllUsers = function(skiprecords){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession'),
        };

        var req = {
          method: 'GET',

          // url:  endpoints.Mobackcollection + '__appUsers?where={\"'+field+'\":true}',
          url:  endpoints.Mobackcollection + '__appUsers?limit=500&skip=' + skiprecords,

          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };



    this.getAllUsersCondCount = function(userid){
      return $q(function (resolve, reject) {

        var searchcondi = {'userId' : {'$regex':'^'+userid+'.*'}};

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession'),
        };

        var req = {
          method: 'GET',
          url:  endpoints.Mobackcollection + '__appUsers?op=count&where=' + encodeURI(JSON.stringify(searchcondi)),
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };



    //https://api.moback.com/objectmgr/api/collections/__appUsers?limit=20&skip=0&where={"userId":{"$regex":"^a.*"}}
    this.getAllUsersSkipBatchCond = function(skiprecords,batchrecords,userid){
      return $q(function (resolve, reject) {


        if (typeof userid == 'undefined'){
          var searchcondi = {"userId":{"$exists":true}};
        }
        else
        {
          var searchcondi = {'userId' : {'$regex':'^'+userid+'.*'}};
        }


        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession'),
        };

        var req = {
          method: 'GET',

          url:  endpoints.Mobackcollection + '__appUsers?limit='+batchrecords+'&skip=' + skiprecords+'&order=userId&where=' + encodeURI(JSON.stringify(searchcondi)),

          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };


    this.getAllUsersSkipBatch = function(skiprecords,batchrecords){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession'),
        };

        var req = {
          method: 'GET',

          // url:  endpoints.Mobackcollection + '__appUsers?where={\"'+field+'\":true}',
          url:  endpoints.Mobackcollection + '__appUsers?limit='+batchrecords+'&skip=' + skiprecords+'&order=userId',

          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };







    this.getUsersWithCondition = function(field,condition,skiprecords){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession'),
        };

        var req = {
          method: 'GET',

         // url:  endpoints.Mobackcollection + '__appUsers?where={\"'+field+'\":true}',
          url:  endpoints.Mobackcollection + '__appUsers?limit=500&skip=' + skiprecords,

          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };


    this.activateAppUserPasswordAccount = function(activationtoken){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json'
        };

        var req = {
          method: 'GET',

          // url:  endpoints.Mobackcollection + '__appUsers?where={\"'+field+'\":true}',
          url:  endpoints.Mobackuser + 'accountactivation/appuser/'+activationtoken,

          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };





    this.changeUserPassword = function(achangetoken,newpassword){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey
        };


        var postData = {
          'password' : newpassword,
          'userId' : achangetoken,
        };


        var req = {
          method: 'POST',
          url:  endpoints.Mobackuser + 'password/change',
          headers: apiHeaders,
          data:postData
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };



    this.enabledisableusers = function(id,flag){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession')
        };

        var putdata = {
          disabled:flag
        };

        var req = {
          method: 'PUT',
          url: endpoints.Mobackcollection + '__appUsers/'+id,
          headers: apiHeaders,
          data : putdata
        };

        apiService.apiCall(req).then(function(data){
          console.log('enable disable user');
          console.log(data);
          if(data.data){
            console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };





    this.assignPartnerId = function(partnerid,useremails){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession')
        };


        var emailfilter = [];
        var emaillist = useremails.split(",");
        _.each(emaillist, function(usremail) {
          var emailobj = {'userId' : usremail};
          emailfilter.push(emailobj);
        });
        var searchcondi = {'$or' : emailfilter};

        var req = {
          method: 'GET',
          url:  endpoints.Mobackcollection + '__appUsers?limit=500&skip=0&where=' + encodeURI(JSON.stringify(searchcondi)),
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){


          if (data.data.results.length >0){

            _.each(data.data.results, function(usreids) {


              try{

                var putdata = {
                  admin: {__type: "Pointer", className: "partners", objectId: partnerid}};

                var ureq = {
                  method: 'PUT',
                  url: endpoints.Mobackcollection + '__appUsers/'+usreids.objectId,
                  headers: apiHeaders,
                  data : putdata
                };

                apiService.apiCall(ureq).then(function(data){
                  if(data.data){
                    console.log(data.data);
                    resolve(data);
                  }
                }, function(data){ reject(data); });

              } catch (e) {

              }
              //self.changedPargnerInUser(partnerid,usreids.objectId);
            });

          }
          if(data.data){
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };



    this.changedPargnerInUser = function(userid,partnerid){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession')
        };


        var putdata = {
          admin: {__type: "Pointer", className: "partners", objectId: partnerid}};

        var req = {
          method: 'PUT',
          url: endpoints.Mobackcollection + '__appUsers/'+userid,
          headers: apiHeaders,
          data : putdata
        };

        apiService.apiCall(req).then(function(data){
          console.log('enable disable user');
          console.log(data);
          if(data.data){
            console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };




    this.getUserByadmin = function(adminid){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession'),
        };

        var searchcondi = {"admin":{"objectId":adminid,"__type":"Pointer","className":"partners"}}

        var req = {
          method: 'GET',
          // url:  endpoints.Mobackcollection + '__appUsers?where=%7B\"userId\"%3A\"johnvogel47%40gmail.com\"%7D',
          url:  endpoints.Mobackcollection + '__appUsers?limit=500&skip=0&where=' + encodeURI(JSON.stringify(searchcondi)),
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };



    this.deletePartnerId = function(useremails){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession')
        };


        var emailfilter = [];
        var emaillist = useremails.split(",");
        _.each(emaillist, function(usremail) {
          var emailobj = {'userId' : usremail};
          emailfilter.push(emailobj);
        });
        var searchcondi = {'$or' : emailfilter};

        var req = {
          method: 'GET',
          url:  endpoints.Mobackcollection + '__appUsers?limit=500&skip=0&where=' + encodeURI(JSON.stringify(searchcondi)),
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){


          if (data.data.results.length >0){

            _.each(data.data.results, function(usreids) {


              try{

                var putdata = {
                  admin: {}};

                var ureq = {
                  method: 'PUT',
                  url: endpoints.Mobackcollection + '__appUsers/'+usreids.objectId,
                  headers: apiHeaders,
                  data : putdata
                };

                apiService.apiCall(ureq).then(function(data){
                  if(data.data){
                    console.log(data.data);
                    resolve(data);
                  }
                }, function(data){ reject(data); });

              } catch (e) {

              }
              //self.changedPargnerInUser(partnerid,usreids.objectId);
            });

          }
          if(data.data){
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };



    //https://api.moback.com/objectmgr/api/collections/__appUsers?limit=20&skip=0&where={"$and":[{"createdAt":{"$gt":{"__type":"Date","iso":"2016-07-01T07:00:39.000Z"}}},{"createdAt":{"$lt":{"__type":"Date","iso":"2016-12-02T07:59:39.000Z"}}}]}

    this.getUserInDateRange = function (fromdate,todate,skiprecords){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession'),
        };

        var searchcondi = {'$and':[{'createdAt':{'$gt':{'__type':'Date','iso':fromdate}}},{'createdAt':{'$lt':{'__type':'Date','iso':todate}}}]}

        var req = {
          method: 'GET',

          // url:  endpoints.Mobackcollection + '__appUsers?where={\"'+field+'\":true}',
          url:  endpoints.Mobackcollection + '__appUsers?limit=500&skip=' + skiprecords + '&where=' + encodeURI(JSON.stringify(searchcondi)),

          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });

    };

    this.getUserInDateRangeCount = function (fromdate,todate){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession'),
        };


        var searchcondi = {'$and':[{'createdAt':{'$gt':{'__type':'Date','iso':fromdate}}},{'createdAt':{'$lt':{'__type':'Date','iso':todate}}}]}

        //var searchcondi = {'userId' : {'$regex':'^'+userid+'.*'}};



        var req = {
          method: 'GET',

          // url:  endpoints.Mobackcollection + '__appUsers?where={\"'+field+'\":true}',
          url:  endpoints.Mobackcollection + '__appUsers?op=count&where=' + encodeURI(JSON.stringify(searchcondi)),

          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            // console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });

    };





    this.resendVerification = function(email){
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'Content-Type' : 'application/json',
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession')
        };

        var req = {
          method: 'POST',
          url: endpoints.MobackSingleUser + 'resend/'+email,
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          if(data.data){
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };

    this.createNotificationdata = function(postData){
      var sessionToken = $cookies.get('superAdminSession');
      return $q(function (resolve, reject) {

        var apiHeaders = {
          'X-Moback-Application-Key' : endpoints.MobackApplicationKey,
          'X-Moback-Environment-Key' : endpoints.MobackEnvironmentKey,
          'X-Moback-SessionToken-Key' : $cookies.get('superAdminSession'),
        };

        var req = {
          method: 'POST',
          url: endpoints.alertNotifications,
          data:postData,
          headers: apiHeaders
        };

        apiService.apiCall(req).then(function(data){
          //console.log('inside service');
          if(data.data){
            console.log(data.data);
            resolve(data);
          }
        }, function(data){ reject(data); });
      });
    };




  });
