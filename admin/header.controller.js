angular.module('admin').controller('AdminHeaderController', function(userService, $state, $cookies){

  var vm = this;
  vm.logout = logout;
  vm.getUserId = getUserId;

  function logout(){
    //submit form
    userService.adminLogout();
    $state.go('admin');
  }

  function getUserId(){
    return $cookies.get('AdminUserId');
  }

});
