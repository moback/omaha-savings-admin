angular.module('omaha', ['ui.bootstrap', 'ui.utils', 'ui.router', 'ngAnimate', 'admin', 'superadmin']);
angular.module('omaha').config(function($stateProvider, $urlRouterProvider) {

    /* Add New States Above */
    //$urlRouterProvider.otherwise('/home');
    $urlRouterProvider.otherwise('/admin');
    //$urlRouterProvider.otherwise('/superadmin');
});

angular.module('omaha').run(function($rootScope) {

    $rootScope.safeApply = function(fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

  $rootScope.$on("$routeChangeSuccess", function(event, currentRoute, previousRoute) {
   // alert(currentRoute);
    $rootScope.title = currentRoute.title;
  });
  /*
  $rootScope.$on('$stateChangeSuccess', function() {
    alert($route.current.title);
   $rootScope.title = $route.current.title;
   // $rootScope.title='WElcome';
  });
*/
});
