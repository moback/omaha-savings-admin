/**
 * Created by user on 3/25/16.
 */
angular.module('admin').controller('recoverpasswordController', function(userService, $state, $cookies){

  var vm = this;
  vm.submitLoginForm = submitForm;

  init();

  function init(){

    if ($cookies.get('AdminSession')){
      //$state.go('superadmin');
    }
  }

  function submitForm(){

    console.log(vm);

    if(!vm.userId){
      window.alert('Please enter your user Id.');
      return;
    }



    //submit form
    userService.recoverPassword(vm).then(function(data){
      //$location.path( '/editProfile' );
      console.log(data.data);
      if (data.data.code == '1000')
      {
        window.alert( 'please check your email for password reset instructions');
      }
      else
      {
        window.alert('Error occured, please try again');
      }


    }, function(data){
      if(data.data && data.data.message){
        //window.alert(data.data.message);
        //toastr.info('Code:' + data.data.code + '. Message: ' + data.data.message);
      }
    });

  }


});


